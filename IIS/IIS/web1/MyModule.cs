﻿using System.Web;
using System.Web.Hosting;
using ProyectoCompi1;

namespace web1
{
    public class MyModule : IHttpHandler
    {
        public bool IsReusable => false;

        public void ProcessRequest(HttpContext context)
        {
            var relativePath = context.Request.FilePath;
            var mapping = HostingEnvironment.MapPath(relativePath);
            var fileContents = System.IO.File.ReadAllText(mapping);
            var compiler = new Compiler(fileContents, 
                context.Request.QueryString, context.Request.Form, context.Request.HttpMethod);
            compiler.Compile();
            context.Response.Write($"{Compiler.Output}");
        }
    }
}
