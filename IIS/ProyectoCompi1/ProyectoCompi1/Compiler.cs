﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    public class Compiler
    {
        public static StringBuilder Output;
        public string File;
        public static string MethodType;
        public static Dictionary<string, dynamic> GetParams;
        public static Dictionary<string, dynamic> FormParams;

        public Compiler(string file, NameValueCollection queryString, 
            NameValueCollection formString, string requestHttpMethod)
        {
            File = file;
            Output = new StringBuilder();
            MethodType = requestHttpMethod;
            GetParams = new Dictionary<string, dynamic>();
            FormParams = new Dictionary<string, dynamic>();
            PopulateGet(queryString);
            PopulateForms(formString);
        }

        public void Compile()
        {
            var lexer = new Lexer.Lexer(File);
            var parser = new Parser(lexer);
            var stmtList = parser.Parse();
            foreach (var stm in stmtList)
            {
                var func = stm as FunctionDeclarationStatement;
                func?.DeclareFunctionSemantic();
            }
            foreach (var stm in stmtList)
            {
                var returnValue = stm.EvaluateSemantic();
                if (returnValue != null && ContainsLabel(returnValue))
                    throw new SemanticException("Loop statement not accepted", stm.CurrentToken);
            }
            foreach (var statementNode in stmtList)
            {
                if(statementNode is FunctionDeclarationStatement)
                    statementNode.Interpret();
            }
            foreach (var statementNode in stmtList)
            {
                if(statementNode is FunctionDeclarationStatement) continue;
                statementNode.Interpret();
            }
            SymbolsTable.Instance.Destroyer();
            ValuesTable.Instance.Destroyer();
        }

        private static bool ContainsLabel(List<TypeNode> types)
        {
            return types.Any(IsLabel);
        }

        private static bool IsLabel(TypeNode type)
        {
            return type is CaseNode || type is DefaultNode
                || type is BreakNode || type is ContinueNode
                || type is ReturnNode;
        }

        public void PopulateForms(NameValueCollection formString)
        {
            foreach (var key in formString.AllKeys)
            {
                var value = formString[key];
                FormParams.Add(key, value);
            }
        }

        private void PopulateGet(NameValueCollection queryString)
        {
            foreach (var key in queryString.AllKeys)
            {
                var value = queryString[key];
                GetParams.Add(key, value);
            }
        }
    }
}
