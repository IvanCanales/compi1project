﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Statements.BasicStatement;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1.Tree.Statements.FunOrVarStatement
{
    public class FunctionDeclarationStatement: ProyectoCompi1.FunOrVarStatement
    {
        public Param Param;
        public StatementNode Statement;

        public FunctionDeclarationStatement(Param param, StatementNode statement)
        {
            Param = param;
            Statement = statement;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            Param?.EvaluateSemantic();
            var returnTypes = Statement?.EvaluateSemantic();
            Param?.DeleteSemantic();
            if(returnTypes != null && !returnTypes.All(CheckReturnNode))
                throw new SemanticException($"Expected return type {Type}", CurrentToken);
            if(returnTypes == null && !(Type is VoidNode))
                throw new SemanticException($"Expected return type {Type}", CurrentToken);
            CheckReturnPaths();
            return RemoveReturnNode(returnTypes);
        }

        private bool CheckReturnNode(TypeNode currentType)
        {
            var returnType = currentType as ReturnNode;
            if (returnType == null) return true;
            return returnType.ReturnType.GetType() == Type.GetType();
        }

        private void CheckReturnPaths()
        {
            if(Type is VoidNode)
                return;
            var compundStm = Statement as CompoundStatementNode;
            if(compundStm != null && compundStm._basicStmList.Any(stm => stm is ReturnStatementNode))
                return;
            throw new SemanticException("Not all paths return a value", CurrentToken);
        }

        private static List<TypeNode> RemoveReturnNode(List<TypeNode> currentReturnTypes)
        {
            return currentReturnTypes?.Where(returnType => !(returnType is ReturnNode)).ToList();
        }

        public void DeclareFunctionSemantic()
        {
            if (SymbolsTable.Instance.FunctionExist(Id.Name))
                throw new SemanticException($" Function: {Id.Name} already exists", CurrentToken);
            SymbolsTable.Instance.DeclareFunction(Id.Name, new Tuple<TypeNode, Param>(Type, Param));
        }

        public override List<ValueNode> Interpret()
        {
            var parameters = Param.GetParamNames();
            ValuesTable.Instance.DeclareFunction(Id.Name, parameters, Statement);
            return new List<ValueNode>();
        }
    }
}
