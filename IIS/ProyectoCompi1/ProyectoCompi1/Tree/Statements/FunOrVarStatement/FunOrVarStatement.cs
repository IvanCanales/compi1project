﻿namespace ProyectoCompi1
{
    public abstract class FunOrVarStatement:StatementNode
    {
        public TypeNode Type;
        public bool IsConst;
        public int Level;
        public IdNode Id;
    }
}