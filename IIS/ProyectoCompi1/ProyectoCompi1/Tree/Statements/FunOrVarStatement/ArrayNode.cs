﻿using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1.Tree.Statements.FunOrVarStatement
{
    public class ArrayNode
    {
        public ExpressionNode Value;
        public ArrayNode NextArray;

        public ArrayNode(ExpressionNode value, ArrayNode nextArr)
        {
            this.Value = value;
            this.NextArray = nextArr;
        }

        public ArrayNode()
        {
            Value = null;
            NextArray = null;
        }

        public void EvaluateSemantic()
        {
            var idnode = Value as IdNode;
            if (idnode != null)
            {
                if (!SymbolsTable.Instance.VariableExist(idnode.Name))
                    throw new SemanticException($"Variable: {idnode.Name} does not exist", Value.CurrentToken);
                if (!(SymbolsTable.Instance.GetVariableType(idnode.Name) is IntegerNode))
                    throw new SemanticException($"Value: {idnode.Name} must be int", Value.CurrentToken);
            }

            NextArray?.EvaluateSemantic();
        }

        public bool HasDimension(int dimension)
        {
            if (dimension == 1)
                return true;
            if (NextArray == null)
                return false;
            return NextArray.HasDimension(dimension - 1);
        }
    }
}
