﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1.Tree.Statements.FunOrVarStatement
{
    public class VarDeclarationStatement: ProyectoCompi1.FunOrVarStatement
    {
        public ArrayNode Array;
        public ExpressionNode Value;
        public List<VarDeclarationStatement> DeclarationList = new List<VarDeclarationStatement>();

        public VarDeclarationStatement(ExpressionNode value)
        {
            Value = value;
        }

        public VarDeclarationStatement()
        {
            Value = null;
        }

        public VarDeclarationStatement(IdNode id, TypeNode type, int level)
        {
            Id = id;
            Type = type;
            Level = level;
            Value = null;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var structNode = Type as StructNode;
            if (structNode != null)
                if(!(SymbolsTable.Instance.StructExist(structNode.Id.Name)))
                    throw new SemanticException($"Struct type: {structNode.Id.Name} not exists", CurrentToken);
            if (SymbolsTable.Instance.VariableExistLastContext(Id.Name))
                throw new SemanticException($" Variable: {Id.Name} already exists", CurrentToken);
            SymbolsTable.Instance.DeclareVariable(Id.Name, this);
            var valueType = Value?.EvaluateSemantic().GetType();
            if (valueType != null && valueType != Type.GetType())
                throw new SemanticException($"Cannot assign: {valueType} to {Type.GetType()}", Value.CurrentToken);
            var valueId = Value as IdNode;
            if (valueId != null)
            {
                var valueLevel = SymbolsTable.Instance.GetVarLevel(valueId.Name);
                if(valueLevel != Level)
                    throw new SemanticException($"cannot assign {Level} Indirection and " +
                                                $"{valueLevel} Indirection", CurrentToken);
            }

            Array?.EvaluateSemantic();

            if (DeclarationList.Count == 0) return new List<TypeNode>();
            foreach (var declaration in DeclarationList)
            {
                declaration.EvaluateSemantic();
            }
            return new List<TypeNode>();
        }

        public bool HasDimension(int dimension)
        {
            return Array != null && Array.HasDimension(dimension);
        }

        public override List<ValueNode> Interpret()
        {
            var value = Value == null ? Type.GetDefaultValue() : Value.Interpret();
            ValuesTable.Instance.DeclareVariable(Id.Name, new VariableValue(value));
            return new List<ValueNode>();
        }
    }
}
