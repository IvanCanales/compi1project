﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    public class Param: VarDeclarationStatement
    {
        public Param nextParam;

        public Param(Param param, Param nextParam)
        {
            this.Type = param.Type;
            this.IsConst = param.IsConst;
            this.Level = param.Level;
            this.Id = param.Id;
            this.Array = param.Array;
            this.nextParam = nextParam;
        }

        public Param(TypeNode type, bool isConst, int level, IdNode id, ArrayNode array, Token currentToken)
        {
            this.Type = type;
            this.IsConst = isConst;
            this.Level = level;
            this.Id = id;
            this.Array = array;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            SymbolsTable.Instance.IncrementStack();
            SymbolsTable.Instance.DeclareVariable(Id.Name, this);
            nextParam?.EvaluateSemantic();
            return null;
        }

        public void DeleteSemantic()
        {
            SymbolsTable.Instance.DecrementStack();
            nextParam?.DeleteSemantic();
        }

        public bool CheckFunCall(ExpressionNode parameters)
        {
            if (Type.GetType() == parameters.EvaluateSemantic().GetType())
            {
                var node = parameters as InlineExpressionNode;
                if (node != null)
                {
                    if (node.NextId == null && nextParam == null)
                        return true;
                    if (nextParam != null && node.NextId != null)
                    {
                        return nextParam.CheckFunCall(node.NextId);
                    }
                }
                else if (nextParam == null)
                    return true;
            }
            return false;
        }

        public List<string> GetParamNames()
        {
            var parameters = new List<string>() {Id.Name};
            if(nextParam != null)
                parameters.AddRange(nextParam.GetParamNames());
            return parameters;
        }
    }
}