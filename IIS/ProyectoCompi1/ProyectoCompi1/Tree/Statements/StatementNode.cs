﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public abstract class StatementNode
    {
        public Token CurrentToken;
        public abstract List<TypeNode> EvaluateSemantic();
        public abstract List<ValueNode> Interpret();
    }
}