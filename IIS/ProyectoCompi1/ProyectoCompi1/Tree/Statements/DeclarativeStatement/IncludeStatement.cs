﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class IncludeStatement : StatementNode
    {
        private string literal;

        public IncludeStatement(string literal, Token currentToken)
        {
            this.literal = literal;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            return null;
        }

        public override List<ValueNode> Interpret()
        {
            return null;
        }
    }
}