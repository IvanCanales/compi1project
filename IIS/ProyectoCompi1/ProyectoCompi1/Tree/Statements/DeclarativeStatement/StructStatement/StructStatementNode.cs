﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    internal class StructStatementNode : StatementNode
    {
        private readonly IdNode _id;
        private readonly StatementNode _statement;

        public StructStatementNode(IdNode id, StatementNode statement, Token currentToken)
        {
            this._id = id;
            this._statement = statement;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            if (SymbolsTable.Instance.StructExist(_id.Name))
                throw new SemanticException($"struct {_id.Name} already exists", CurrentToken);
            _statement.EvaluateSemantic();
            var stm = _statement as StructCompoundStatement;
            SymbolsTable.Instance.DeclareStruct(_id.Name, stm?.Attributes);

            return null;
        }

        public override List<ValueNode> Interpret()
        {
            throw new NotImplementedException();
        }
    }
}