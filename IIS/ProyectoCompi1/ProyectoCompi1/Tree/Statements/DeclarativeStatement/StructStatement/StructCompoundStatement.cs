﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    internal class StructCompoundStatement : StatementNode
    {
        public List<VarDeclarationStatement> Attributes;

        public StructCompoundStatement(List<VarDeclarationStatement> attributes, Token currentToken)
        {
            this.Attributes = attributes;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            SymbolsTable.Instance.IncrementStack();
            foreach (var attribute in Attributes)
            {
                var attributeVar = attribute as VarDeclarationStatement;
                attributeVar?.EvaluateSemantic();
            }
            SymbolsTable.Instance.DecrementStack();
            return null;
        }

        public override List<ValueNode> Interpret()
        {
            throw new System.NotImplementedException();
        }
    }
}