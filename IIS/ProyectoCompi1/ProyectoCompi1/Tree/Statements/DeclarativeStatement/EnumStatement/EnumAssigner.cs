﻿using ProyectoCompi1.Arbol.Expression;

namespace ProyectoCompi1
{
    public class EnumAssigner
    {
        public IdNode id;
        public ExpressionNode value;
        public EnumAssigner nextAssigner;

        public EnumAssigner(ExpressionNode value, EnumAssigner nextAssigner)
        {
            this.value = value;
            this.nextAssigner = nextAssigner;
        }
    }
}