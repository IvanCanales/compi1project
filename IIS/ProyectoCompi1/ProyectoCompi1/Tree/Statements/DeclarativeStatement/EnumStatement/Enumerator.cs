﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;

namespace ProyectoCompi1
{
    public class Enumerator
    {
        public IdNode id;
        public ExpressionNode value;
        public Enumerator nextEnum;
        public Token CurrentToken;

        public Enumerator(ExpressionNode value, Enumerator nextEnum)
        {
            this.value = value;
            this.nextEnum = nextEnum;
        }

        public List<string> EvaluateSemantic()
        {
            if (value != null)
            {
                var expType = value.EvaluateSemantic();
                if (!(expType is IntegerNode || expType is CharacterNode))
                    throw new SemanticException("Enum can only be assigned int or char", CurrentToken);
            }
            var newIds = new List<string>();
            if (nextEnum != null)
            {
                var ids = nextEnum.EvaluateSemantic();
                if (ids.Contains(id.Name))
                    throw new SemanticException("Duplicate ids", CurrentToken);
                newIds.AddRange(ids);
            }
            newIds.Add(id.Name);
            return newIds;
        }

        public bool IdExists(string name)
        {
            if (id.Name == name)
                return true;
            if (nextEnum != null)
                return nextEnum.IdExists(name);
            return false;
        }
    }
}