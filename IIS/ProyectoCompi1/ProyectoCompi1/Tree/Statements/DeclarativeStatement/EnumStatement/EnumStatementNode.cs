﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    public class EnumStatementNode: StatementNode
    {
        public IdNode Id;
        public Enumerator enumerator;
        public EnumAssigner assigner;

        public EnumStatementNode(Enumerator enumerator, EnumAssigner asssigner)
        {
            this.enumerator = enumerator;
            this.assigner = asssigner;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            if (SymbolsTable.Instance.EnumExist(Id.Name))
                throw new SemanticException($"struct {Id.Name} already exists", CurrentToken);
            enumerator.EvaluateSemantic();
            SymbolsTable.Instance.DeclareEnum(Id.Name, enumerator);
            return null;
        }

        public override List<ValueNode> Interpret()
        {
            throw new System.NotImplementedException();
        }
    }
}