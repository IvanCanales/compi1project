﻿using System.Collections.Generic;
using System.Linq;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    internal class ReturnStatementNode : StatementNode
    {
        private readonly StatementNode _statement;

        public ReturnStatementNode(StatementNode statement, Token currentToken)
        {
            _statement = statement;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var returnType = _statement?.EvaluateSemantic();
            var returnNode = new ReturnNode();
            if (returnType != null)
            {
                returnNode = new ReturnNode(returnType.First());
            }
            return new List<TypeNode>() { returnNode };
        }

        public override List<ValueNode> Interpret()
        {
            var returnValue = _statement.Interpret();
            var returnNode = new ReturnValue();
            if (returnValue != null)
            {
                returnNode = new ReturnValue(returnValue.First());
            }
            return new List<ValueNode>() { returnNode };
        }
    }
}