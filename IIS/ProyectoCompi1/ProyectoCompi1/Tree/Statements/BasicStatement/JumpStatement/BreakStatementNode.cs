﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class BreakStatementNode : StatementNode
    {
        public BreakStatementNode(Token currentToken)
        {
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var breakType = new List<TypeNode> {new BreakNode()};
            return breakType;
        }

        public override List<ValueNode> Interpret()
        {
            var breakValue = new List<ValueNode> { new BreakValue() };
            return breakValue;
        }
    }
}