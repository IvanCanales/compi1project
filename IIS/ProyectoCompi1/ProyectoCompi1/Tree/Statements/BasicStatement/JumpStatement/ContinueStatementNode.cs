﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class ContinueStatementNode : StatementNode
    {
        public ContinueStatementNode(Token currentToken)
        {
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var continueType = new List<TypeNode> {new ContinueNode()};
            return continueType;
        }

        public override List<ValueNode> Interpret()
        {
            var continueValue = new List<ValueNode> { new ContinueValue() };
            return continueValue;
        }
    }
}