﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    internal class PrintStatement : StatementNode
    {
        private readonly ExpressionNode _expression;

        public PrintStatement(ExpressionNode expression, Token _currentToken)
        {
            this._expression = expression;
            this.CurrentToken = _currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var idnode = _expression as IdNode;
            if (idnode != null)
            {
                if(!SymbolsTable.Instance.VariableExist(idnode.Name))
                    throw new SemanticException($"Variable {idnode.Name} not exist", idnode.CurrentToken);
            }
            return new List<TypeNode>();
        }

        public override List<ValueNode> Interpret()
        {
            dynamic value = _expression.Interpret();
            string output = Regex.Unescape(value.Value.ToString());
            Compiler.Output.Append(output);
            return new List<ValueNode>();
        }
    }
}