﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class HtmlStatement : StatementNode
    {
        private string _htmlTag;

        public HtmlStatement(string tag, Token currentToken)
        {
            this._htmlTag = tag;
            this.CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            return new List<TypeNode>();
        }

        public override List<ValueNode> Interpret()
        {
            Compiler.Output.Append(_htmlTag);
            return new List<ValueNode>();
        }
    }
}