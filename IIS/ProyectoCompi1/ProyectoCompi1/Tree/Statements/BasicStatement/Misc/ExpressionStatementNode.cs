using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class ExpressionStatementNode: StatementNode
    {
        public ExpressionNode Expression;

        public ExpressionStatementNode(Token currentToken)
        {
            this.Expression = new EndOfStatementNode();
            CurrentToken = currentToken;
        }

        public ExpressionStatementNode(ExpressionNode exp, Token currentToken)
        {
            this.Expression = exp;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            return new List<TypeNode>() { Expression.EvaluateSemantic()};
        }

        public override List<ValueNode> Interpret()
        {
            var list = new List<ValueNode> {Expression.Interpret()};
            return list;
        }
    }
}