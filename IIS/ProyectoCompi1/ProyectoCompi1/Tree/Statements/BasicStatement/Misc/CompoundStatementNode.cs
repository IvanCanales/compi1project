﻿using System.Collections.Generic;
using System.Linq;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1.Tree.Statements.BasicStatement
{
    internal class CompoundStatementNode : StatementNode
    {
        public readonly List<StatementNode> _basicStmList;

        public CompoundStatementNode(List<StatementNode> basicStmList, Token currentToken)
        {
            _basicStmList = basicStmList;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            List<TypeNode> returnValue = null;
            if (_basicStmList == null) return null;
            SymbolsTable.Instance.IncrementStack();
            foreach (var stm in _basicStmList)
            {
                var stmType = stm.EvaluateSemantic();
                if (returnValue == null)
                    returnValue = stmType;
                else
                {
                    returnValue = CompareTypes(returnValue, stmType, stm.CurrentToken);
                    if (returnValue == null)
                        throw new SemanticException("Cannot contain loop statement with this types", stm.CurrentToken);
                }
            }
            SymbolsTable.Instance.DecrementStack();
            return returnValue;
        }

        public static List<TypeNode> CompareTypes(List<TypeNode> returnTypesLeft, List<TypeNode> returnTypesRight, Token errorToken)
        {
            TypeNode currentReturnType = null;
            var newReturnTypes = new List<TypeNode>();
            var addNewType = true;
            foreach (var returnType in returnTypesLeft)
            {
                var returnNode = returnType as ReturnNode;
                if (returnNode != null)
                    currentReturnType = returnNode.ReturnType;
                newReturnTypes.Add(returnType);
            }
            foreach (var returnType in returnTypesRight)
            {
                if (!IsLoopType(returnType) && currentReturnType != null)
                {
                    var returnNode = returnType as ReturnNode;
                    if (returnNode != null &&
                        currentReturnType.GetType() != returnNode.ReturnType.GetType())
                    {
                        return new List<TypeNode>();
                    }
                }
                else
                {
                    if (IsLoopType(returnType) &&
                        returnTypesLeft.Any(returnValue => returnValue.GetType() == returnType.GetType()))
                    {
                            addNewType = false;
                    }
                    if (addNewType)
                        newReturnTypes.Add(returnType);
                    addNewType = true;
                }
            }
            return newReturnTypes;
        }

        private static bool IsLoopType(TypeNode typeNode)
        {
            return typeNode is CaseNode || typeNode is DefaultNode 
                || typeNode is ContinueNode || typeNode is BreakNode;
        }

        public override List<ValueNode> Interpret()
        {
            var returnValue = new List<ValueNode>();
            if (_basicStmList == null) return returnValue;
            ValuesTable.Instance.IncrementStack();
            foreach (var stm in _basicStmList)
            {
                var stmValue = stm.Interpret();
                returnValue.AddRange(stmValue);
                if(ContainsLoopBreak(stmValue))
                    break;
            }
            ValuesTable.Instance.DecrementStack();
            return returnValue;
        }

        private bool ContainsLoopBreak(List<ValueNode> stmValue)
        {
            return stmValue.Any(valueNode => IsLoopBreak(valueNode));
        }

        private bool IsLoopBreak(ValueNode valueNode)
        {
            return valueNode is ReturnValue || valueNode is BreakValue || valueNode is ContinueValue;
        }
    }
}