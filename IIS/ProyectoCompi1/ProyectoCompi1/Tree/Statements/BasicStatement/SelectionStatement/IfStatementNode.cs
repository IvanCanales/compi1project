﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Statements.BasicStatement;

namespace ProyectoCompi1
{
    public class IfStatementNode: StatementNode
    {
        public ExpressionNode Condition;
        public StatementNode Statement;
        public StatementNode ElseStatement;

        public IfStatementNode(ExpressionNode condition, StatementNode statement, StatementNode elseStatement)
        {
            this.Condition = condition;
            this.Statement = statement;
            this.ElseStatement = elseStatement;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            List<TypeNode> returnValue;
            var conditionType = Condition.EvaluateSemantic();
            if (!(conditionType is BooleanNode))
                throw new SemanticException($"Cannot if: {conditionType}", CurrentToken);
            var ifType = Statement.EvaluateSemantic();
            var elseType = ElseStatement?.EvaluateSemantic();
            if(ElseStatement != null)
            {
                returnValue = CompoundStatementNode.CompareTypes(ifType, elseType, ElseStatement.CurrentToken);
                if (returnValue == null)
                    throw new SemanticException("Cannot contain loop statement with this types", ElseStatement.CurrentToken);
            }
            else
            {
                returnValue = ifType;
            }
            return returnValue;
        }

        public override List<ValueNode> Interpret()
        {
            var returnValue = new List<ValueNode>();
            var condition = (BooleanValue)Condition.Interpret();
            if (condition.Value)
            {
                var stmVal = Statement.Interpret();
                returnValue.AddRange(stmVal);
                if (ContainsLoopBreak(returnValue))
                    return returnValue;
            }
            else if(ElseStatement != null)
                returnValue.AddRange(ElseStatement.Interpret());
            return returnValue;
        }

        private bool ContainsLoopBreak(List<ValueNode> returnValue)
        {
            foreach (var valueNode in returnValue)
            {
                if (IsLoopBreak(valueNode))
                    return true;
            }
            return false;
        }

        private bool IsLoopBreak(ValueNode value)
        {
            return value is ContinueValue || value is ReturnValue || value is BreakValue;
        }
    }
}