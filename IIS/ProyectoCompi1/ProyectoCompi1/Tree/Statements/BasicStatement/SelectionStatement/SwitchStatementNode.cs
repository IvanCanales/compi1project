﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Statements.BasicStatement;

namespace ProyectoCompi1
{
    internal class SwitchStatementNode : StatementNode
    {
        private readonly StatementNode _basicStatement;
        private readonly ExpressionNode _expression;

        public SwitchStatementNode(ExpressionNode exp, StatementNode basicStatement, Token currentToken)
        {
            this._expression = exp;
            this._basicStatement = basicStatement;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var expType = _expression.EvaluateSemantic();
            if (expType is StructNode || expType is DateNode || expType is VoidNode)
                throw new SemanticException($"Switch cannot contain {expType}", CurrentToken);
            var compoundStm = _basicStatement as CompoundStatementNode;
            if(compoundStm == null)
                throw new SemanticException("Expected { ", CurrentToken);
            if(!compoundStm._basicStmList.All(statement => statement is CaseStatementNode || statement is DefaultStatementNode))
                throw new SemanticException("Switch can only contain case and default. ", CurrentToken);
            var types = _basicStatement?.EvaluateSemantic();
            return RemoveLabels(types, expType, _expression.CurrentToken);
        }

        private List<TypeNode> RemoveLabels(List<TypeNode> types, TypeNode expType, Token expToken)
        {
            var returnTypes = new List<TypeNode>();
            foreach (var type in types)
            {
                var caseNode = type as CaseNode;
                if (caseNode != null)
                {
                    if(caseNode.CaseType.GetType() != expType.GetType())
                        throw new SemanticException("Case incorrect type", expToken);
                }
                if(!IsSwitchLabel(type))
                    returnTypes.Add(type);
            }
            return returnTypes;
        }

        public static bool IsSwitchLabel(TypeNode type)
        {
            return type is CaseNode || type is DefaultNode || type is BreakNode;
        }

        public override List<ValueNode> Interpret()
        {
            throw new NotImplementedException();
        }
    }
}