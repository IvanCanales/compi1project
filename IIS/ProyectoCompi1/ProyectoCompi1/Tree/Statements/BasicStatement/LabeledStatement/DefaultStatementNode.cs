﻿using System.Collections.Generic;
using System.Linq;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Statements.BasicStatement;
using ProyectoCompi1.Tree.Statements.BasicStatement.LabeledStatement;

namespace ProyectoCompi1
{
    public class DefaultStatementNode : LabeledStatementNode
    {
        public DefaultStatementNode(List<StatementNode> basicStatementList, Token currentToken)
        {
            this.BasicStatementList = basicStatementList;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var hasBreak = false;
            List<TypeNode> returnValue = null;
            if (BasicStatementList == null)
                throw new SemanticException("Break statement expected", CurrentToken);
            foreach (var stm in BasicStatementList)
            {
                var stmType = stm.EvaluateSemantic();
                if (HasBreak(stmType))
                    hasBreak = true;
                if (returnValue == null)
                    returnValue = stmType;
                else
                {
                    returnValue = CompoundStatementNode.CompareTypes(returnValue, stmType, stm.CurrentToken);
                    if (returnValue == null)
                        throw new SemanticException("Cannot contain loop statement with this types", stm.CurrentToken);
                }
                if (stm is DefaultStatementNode)
                    throw new SemanticException("Multiple default found", stm.CurrentToken);
                if(stm is CaseStatementNode)
                    throw new SemanticException("No case can go after default. ", stm.CurrentToken);
            }
            if(!hasBreak)
                throw new SemanticException("Expected break statement", CurrentToken);
            if(returnValue == null)
                returnValue = new List<TypeNode>();
            returnValue.Add(new DefaultNode());
            return returnValue;
        }

        public override List<ValueNode> Interpret()
        {
            throw new System.NotImplementedException();
        }

        public static bool HasBreak(List<TypeNode> types)
        {
            return types.Any(type => type is BreakNode);
        }
    }
}