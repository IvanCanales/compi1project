﻿using System.Collections.Generic;

namespace ProyectoCompi1.Tree.Statements.BasicStatement.LabeledStatement
{
    public abstract class LabeledStatementNode: StatementNode
    {
        public List<StatementNode> BasicStatementList;
        public abstract override List<TypeNode> EvaluateSemantic();
    }
}
