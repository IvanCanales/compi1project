﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Statements.BasicStatement;
using ProyectoCompi1.Tree.Statements.BasicStatement.LabeledStatement;

namespace ProyectoCompi1
{
    internal class CaseStatementNode : LabeledStatementNode
    {
        private readonly ExpressionNode _expression;

        public CaseStatementNode(ExpressionNode expression, List<StatementNode> basicStatementList, Token currentToken)
        {
            _expression = expression;
            this.BasicStatementList = basicStatementList;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var expType = _expression.EvaluateSemantic();
            var hasBreak = false;
            List<TypeNode> returnValue = null;
            if (BasicStatementList == null)
                throw new SemanticException("Break statement expected", CurrentToken);
            foreach (var stm in BasicStatementList)
            {
                var stmType = stm.EvaluateSemantic();
                if (DefaultStatementNode.HasBreak(stmType))
                    hasBreak = true;
                if (returnValue == null)
                    returnValue = stmType;
                else
                {
                    returnValue = CompoundStatementNode.CompareTypes(returnValue, stmType, stm.CurrentToken);
                    if (returnValue == null)
                        throw new SemanticException("Cannot contain loop statement with this types", stm.CurrentToken);
                }
            }
            if (!hasBreak)
                throw new SemanticException("Expected break statement", CurrentToken);
            if (returnValue == null)
                returnValue = new List<TypeNode>();
            returnValue.Add(new CaseNode(expType));
            return returnValue;
        }

        public override List<ValueNode> Interpret()
        {
            throw new System.NotImplementedException();
        }
    }
}