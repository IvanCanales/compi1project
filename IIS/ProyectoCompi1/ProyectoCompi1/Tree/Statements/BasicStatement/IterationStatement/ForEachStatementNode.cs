﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using ProyectoCompi1.Arbol.Statements.IterationStatement;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    internal class ForEachStatementNode : IterationStatementNode
    {
        private readonly IdNode _id;
        private readonly int _level;
        private readonly IdNode _listId;
        private readonly TypeNode _type;

        public ForEachStatementNode(TypeNode type, int level, IdNode id, IdNode listId, Token currentToken)
        {
            this._type = type;
            this._level = level;
            this._id = id;
            this._listId = listId;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            SymbolsTable.Instance.IncrementStack();
            SymbolsTable.Instance.DeclareVariable(_id.Name, 
                new VarDeclarationStatement(_id, _type, _level));
            if (!SymbolsTable.Instance.VariableExist(_listId.Name))
                throw new SemanticException($" Variable: {_id.Name} not exists", CurrentToken);
            var listType = SymbolsTable.Instance.GetVariableType(_listId.Name);
            if (_type.GetType() != listType.GetType())
                throw new SemanticException($"Cannot foreach: {_type} and {listType}", CurrentToken);
            var types = BasicStatement?.EvaluateSemantic();
            SymbolsTable.Instance.DecrementStack();
            return types != null ? RemoveLabels(types) : null;
        }

        public override List<ValueNode> Interpret()
        {
            throw new NotImplementedException();
        }
    }
}