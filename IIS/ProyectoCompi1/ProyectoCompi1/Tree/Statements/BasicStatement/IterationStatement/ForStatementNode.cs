﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Arbol.Statements.IterationStatement;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class ForStatementNode : IterationStatementNode
    {
        private readonly ExpressionNode _declarativeExpression;
        private readonly ExpressionNode _counterExpression;

        public ForStatementNode(ExpressionNode declarativeExpression, ExpressionNode conditionalExpression
            , ExpressionNode counterExpression, Token currentToken)
        {
            this._declarativeExpression = declarativeExpression;
            this.ConditionalExpression = conditionalExpression;
            this._counterExpression = counterExpression;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            _declarativeExpression?.EvaluateSemantic();
            var conditionType = ConditionalExpression?.EvaluateSemantic();
            if (conditionType != null && !(conditionType is BooleanNode))
                throw new SemanticException($"Expected bool found {conditionType}", CurrentToken);
            _counterExpression?.EvaluateSemantic();
            var types = BasicStatement?.EvaluateSemantic();
            return types != null? RemoveLabels(types) : null;
        }

        public override List<ValueNode> Interpret()
        {
            _declarativeExpression?.Interpret();
            var condition = (BooleanValue)ConditionalExpression?.Interpret();
            var types = new List<ValueNode>();
            while (condition == null || condition.Value)
            {
                if(BasicStatement != null)
                    types.AddRange(BasicStatement.Interpret());
                if (ContainsLoopBreakValue(types))
                    break;
                _counterExpression?.Interpret();
                condition = (BooleanValue)ConditionalExpression?.Interpret();
            }
            types = RemoveLoopValues(types);
            return types;
        }
    }
}