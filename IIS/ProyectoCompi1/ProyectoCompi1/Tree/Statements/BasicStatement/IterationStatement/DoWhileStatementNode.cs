﻿using System.Collections.Generic;
using System.ComponentModel;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Arbol.Statements.IterationStatement;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class DoWhileStatementNode : IterationStatementNode
    {
        public DoWhileStatementNode(ExpressionNode expression, StatementNode basicStatement, Token currentToken)
        {
            this.ConditionalExpression = expression;
            this.BasicStatement = basicStatement;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var conditionalType = ConditionalExpression.EvaluateSemantic();
            if (!(conditionalType is BooleanNode))
                throw new SemanticException($"cannot do while with condition: {conditionalType}", CurrentToken);
            var types = BasicStatement?.EvaluateSemantic();
            return types != null ? RemoveLabels(types) : null;
        }

        public override List<ValueNode> Interpret()
        {
            var condition = (BooleanValue)ConditionalExpression.Interpret();
            var types = new List<ValueNode>();
            do
            {
                if(BasicStatement != null)
                    types.AddRange(BasicStatement.Interpret());
                if(ContainsLoopBreakValue(types))
                    break;
                condition = (BooleanValue)ConditionalExpression.Interpret();
            } while (condition.Value);
            types = RemoveLoopValues(types);
            return types;
        }
    }
}