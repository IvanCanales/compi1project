﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1.Arbol.Statements.IterationStatement
{
    abstract class IterationStatementNode: StatementNode
    {
        public StatementNode BasicStatement;
        public ExpressionNode ConditionalExpression;
        public abstract override List<TypeNode> EvaluateSemantic();

        protected List<TypeNode> RemoveLabels(List<TypeNode> types)
        {
            var returnTypes = new List<TypeNode>();
            foreach (var type in types)
            {
                if (IsLoopLabel(type))
                    continue;
                if (SwitchStatementNode.IsSwitchLabel(type))
                    throw new SemanticException($"Cannot contain {type}", CurrentToken);
                returnTypes.Add(type);
            }
            return returnTypes;
        }

        protected bool IsLoopLabel(TypeNode type)
        {
            return type is BreakNode || type is ContinueNode;
        }

        protected bool ContainsLoopBreakValue(List<ValueNode> values)
        {
            foreach (var valueNode in values)
            {
                if (valueNode is BreakValue || valueNode is ReturnValue)
                    return true;
            }
            return false;
        }

        protected List<ValueNode> RemoveLoopValues(List<ValueNode> values)
        {
            var newValues = new List<ValueNode>();
            foreach (var valueNode in values)
            {
                if(IsLoopValue(valueNode))
                    continue;
                newValues.Add(valueNode);
            }
            return newValues;
        }

        private bool IsLoopValue(ValueNode valueNode)
        {
            return valueNode is ContinueValue || valueNode is BreakValue;
        }
    }
}
