﻿using System.Collections.Generic;
using System.Xml.Xsl.Runtime;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Arbol.Statements.IterationStatement;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class WhileStatementNode : IterationStatementNode
    {
        public WhileStatementNode(ExpressionNode expression, StatementNode basicStatement, Token currentToken)
        {
            this.ConditionalExpression = expression;
            this.BasicStatement = basicStatement;
            CurrentToken = currentToken;
        }

        public override List<TypeNode> EvaluateSemantic()
        {
            var conditionType = ConditionalExpression.EvaluateSemantic();
            if (!(conditionType is BooleanNode))
                throw new SemanticException($"Cannot while: {conditionType}", CurrentToken);
            var types = BasicStatement?.EvaluateSemantic();
            return types != null ? RemoveLabels(types) : new List<TypeNode>();
        }

        public override List<ValueNode> Interpret()
        {
            var condition = (BooleanValue)ConditionalExpression.Interpret();
            var types = new List<ValueNode>();
            while (condition.Value)
            {
                if(BasicStatement != null)
                    types.AddRange(BasicStatement.Interpret());
                if (ContainsLoopBreakValue(types))
                    break;
                condition = (BooleanValue)ConditionalExpression.Interpret();
            }
            types = RemoveLoopValues(types);
            return types;
        }
    }
}