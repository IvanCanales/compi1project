﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class GetFormFunc : ExpressionNode
    {
        public ExpressionNode expression;

        public GetFormFunc(ExpressionNode exp)
        {
            expression = exp;
        }

        public override TypeNode EvaluateSemantic()
        {
            if (!(expression.EvaluateSemantic() is StringNode))
                throw new SemanticException("Parameter must be of string type", expression.CurrentToken);
            return new StringNode();
        }

        public override ValueNode Interpret()
        {
            var paramName = (StringValue)expression.Interpret();
            var value = Compiler.FormParams[paramName.Value];
            return new StringValue() { Value = value };
        }
    }
}