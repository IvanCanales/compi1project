﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class GetMethodFunc : ExpressionNode
    {
        public override TypeNode EvaluateSemantic()
        {
            return new VoidNode();
        }

        public override ValueNode Interpret()
        {
            return new StringValue() {Value = Compiler.MethodType};
        }
    }
}