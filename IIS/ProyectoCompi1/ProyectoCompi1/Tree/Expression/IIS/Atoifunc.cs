﻿using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class Atoifunc : ExpressionNode
    {
        private ExpressionNode exp;

        public Atoifunc(ExpressionNode exp)
        {
            this.exp = exp;
        }

        public override TypeNode EvaluateSemantic()
        {
            if(!(exp.EvaluateSemantic() is StringNode))
                throw new SemanticException("param must be string", exp.CurrentToken);
            return new IntegerNode();
        }

        public override ValueNode Interpret()
        {
            var value = (StringValue)exp.Interpret();
            int intValue = int.Parse(value.Value);
            return new IntegerValue() {Value = intValue};
        }
    }
}