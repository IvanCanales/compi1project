﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1.Arbol.Expression
{
    public abstract class ExpressionNode
    {
        public Token CurrentToken;
        public abstract TypeNode EvaluateSemantic();
        public abstract ValueNode Interpret();
    }
}