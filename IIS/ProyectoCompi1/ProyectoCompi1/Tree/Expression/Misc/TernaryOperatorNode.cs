﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class TernaryOperatorNode: ExpressionNode
    {
        public ExpressionNode ConditionalExpression { get; set; }
        public ExpressionNode TrueExpression { get; set; }
        public ExpressionNode FalseExpression { get; set; }

        public TernaryOperatorNode(ExpressionNode conditionalExpression, ExpressionNode trueExpression, ExpressionNode falseExpression)
        {
            this.ConditionalExpression = conditionalExpression;
            this.TrueExpression = trueExpression;
            this.FalseExpression = falseExpression;
        }

        public override TypeNode EvaluateSemantic()
        {
            var condType = ConditionalExpression.EvaluateSemantic();
            var falseType = FalseExpression.EvaluateSemantic();
            var trueType = TrueExpression.EvaluateSemantic();
            if (Valid(condType) && trueType.GetType() == falseType.GetType())
            {
                return trueType;
            }
            throw new SemanticException($"Cannot ternary: {condType} ? {trueType} : {falseType}", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            var condition = (BooleanValue)ConditionalExpression.Interpret();
            var value = condition.Value ? TrueExpression.Interpret() : FalseExpression.Interpret();
            return value;
        }

        private bool Valid(TypeNode node)
        {
            return !(node is StructNode) && !(node is DateNode) && !(node is VoidNode);
        }
    }
}