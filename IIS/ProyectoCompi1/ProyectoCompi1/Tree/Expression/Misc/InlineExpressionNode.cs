﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class InlineExpressionNode : ExpressionNode
    {
        public readonly ExpressionNode CurrentId;
        public readonly ExpressionNode NextId;

        public InlineExpressionNode(ExpressionNode currentId, ExpressionNode nextId)
        {
            CurrentId = currentId;
            NextId = nextId;
        }

        public override TypeNode EvaluateSemantic()
        {
            var currentType = CurrentId.EvaluateSemantic();
            NextId?.EvaluateSemantic();
            return currentType;
        }

        public override ValueNode Interpret()
        {
            return new VoidValue();
        }

        public List<ValueNode> GetValues()
        {
            var currentIdnode = CurrentId as IdNode;
            var value = ValuesTable.Instance.GetVariableValue(currentIdnode.Name);
            var list = new List<ValueNode> {value};
            var inline = NextId as InlineExpressionNode;
            if(inline != null)
                list.AddRange(inline.GetValues());
            var idnode = NextId as IdNode;
            if (idnode == null) return list;
            value = ValuesTable.Instance.GetVariableValue(idnode.Name);
            list.Add(value);
            return list;
        }
    }
}