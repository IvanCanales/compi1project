﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class ArrayInitializer : ExpressionNode
    {
        private readonly ExpressionNode _value;
        private readonly ExpressionNode _nextValue;

        public ArrayInitializer(ExpressionNode value, ExpressionNode nextValue, Token currentToken)
        {
            this._value = value;
            this._nextValue = nextValue;
            CurrentToken = currentToken;
        }

        public override TypeNode EvaluateSemantic()
        {
            var valueType = _value.EvaluateSemantic();
            var nextValueType = _nextValue.EvaluateSemantic();
            if (valueType.GetType() == nextValueType.GetType())
            {
                return valueType;
            }
            throw new SemanticException($"Cannot init array: {valueType} and {nextValueType}", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            throw new NotImplementedException();
        }
    }
}