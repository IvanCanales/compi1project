﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class ParenthesisExpressionNode : ExpressionNode
    {
        private readonly ExpressionNode _accessLink;
        private readonly ExpressionNode _expression;

        public ParenthesisExpressionNode(ExpressionNode expression, ExpressionNode accessLink)
        {
            this._expression = expression;
            this._accessLink = accessLink;
        }

        public override TypeNode EvaluateSemantic()
        {
            return _accessLink != null ? _accessLink.EvaluateSemantic() : _expression.EvaluateSemantic();
        }

        public override ValueNode Interpret()
        {
            throw new System.NotImplementedException();
        }
    }
}