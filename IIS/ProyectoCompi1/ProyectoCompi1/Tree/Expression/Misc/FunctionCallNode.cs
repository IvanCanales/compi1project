﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    internal class FunctionCallNode : ExpressionNode
    {
        private readonly ExpressionNode _params;
        private readonly IdNode _id;

        public FunctionCallNode(ExpressionNode name, ExpressionNode _params)
        {
            this._id = (IdNode)name;
            this._params = _params;
        }

        public override TypeNode EvaluateSemantic()
        {
            if (!SymbolsTable.Instance.FunctionExist(_id.Name))
                throw new SemanticException($"function {_id.Name} not exists", CurrentToken);
            return SymbolsTable.Instance.GetFunction(_id.Name, _params, CurrentToken);
        }

        public override ValueNode Interpret()
        {
            if (_params is InlineExpressionNode)
            {
                var parameters = ((InlineExpressionNode)_params).GetValues();
                return ValuesTable.Instance.CallFunction(_id.Name, parameters);
            }
            var idnode = _params as IdNode;
            ValueNode value;
            if (idnode != null)
            {
                value = ValuesTable.Instance.GetVariableValue(idnode.Name);
                return ValuesTable.Instance.CallFunction(_id.Name, new List<ValueNode>()
                {
                    value
                });
            }
            if(_params == null)
                return ValuesTable.Instance.CallFunction(_id.Name, new List<ValueNode>());

            value = _params.Interpret();
            return ValuesTable.Instance.CallFunction(_id.Name, new List<ValueNode>()
            {
                value
            });
        }
    }
}