﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    public class IdNode : ExpressionNode
    {
        public readonly string Name;

        public IdNode(string id, Token token)
        {
            Name = id;
            CurrentToken= token;
        }

        public override TypeNode EvaluateSemantic()
        {
            var variable = SymbolsTable.Instance.GetVariableType(Name);
            if (variable != null)
                return variable;
            if(SymbolsTable.Instance.EnumExist(Name))
                return new EnumNode();
            throw new SemanticException("variable or Enum not exist ", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            return ValuesTable.Instance.GetVariableValue(Name);
        }
    }
}