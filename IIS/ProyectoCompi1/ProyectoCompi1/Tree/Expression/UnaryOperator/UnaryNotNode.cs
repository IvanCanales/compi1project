﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class UnaryNotNode : UnaryOperatorNode
    {
        public override TypeNode EvaluateSemantic()
        {
            var type = Operand.EvaluateSemantic();
            if (Valid(type))
            {
                return new BooleanNode();
            }
            throw new SemanticException($"Cannot not: {type}", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            var leftValue = Operand.Interpret();
            var value = GetValue(leftValue);

            var idnode = Operand as IdNode;
            ValuesTable.Instance.SetVariableValue(idnode.Name, value);
            return value;
        }

        private bool Valid(TypeNode node)
        {
            return node is BooleanNode;
        }

        private ValueNode GetValue(ValueNode value)
        {
            var boolValue = value as BooleanValue;
            return new BooleanValue { Value = !boolValue.Value };
        }
    }
}