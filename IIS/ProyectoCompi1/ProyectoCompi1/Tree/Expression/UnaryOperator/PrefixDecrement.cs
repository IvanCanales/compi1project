﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class PrefixDecrement : UnaryOperatorNode
    {
        public PrefixDecrement(ExpressionNode unaryExp)
        {
            this.Operand = unaryExp;
        }

        public override TypeNode EvaluateSemantic()
        {
            var type = Operand.EvaluateSemantic();
            var returnType = Valid(type);
            if (returnType != null)
            {
                return returnType;
            }
            throw new SemanticException($"Cannot preDec: {type}", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            var leftValue = Operand.Interpret();
            var value = GetValue(leftValue);

            var idnode = Operand as IdNode;
            ValuesTable.Instance.SetVariableValue(idnode.Name, value);
            return value;
        }

        private TypeNode Valid(TypeNode type)
        {
            if (type is FloatNode)
                return new FloatNode();
            if (type is IntegerNode)
                return new IntegerNode();
            if (type is CharacterNode)
                return new CharacterNode();
            return null;
        }

        private ValueNode GetValue(ValueNode value)
        {
            var intValue = value as IntegerValue;
            var floatValue = value as FloatValue;

            if (intValue != null)
            {
                return new IntegerValue { Value = intValue.Value - 1 };
            }
            return new FloatValue { Value = floatValue.Value - 1 };
        }
    }
}