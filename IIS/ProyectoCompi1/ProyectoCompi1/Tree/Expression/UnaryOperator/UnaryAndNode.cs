﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class UnaryAndNode : UnaryOperatorNode
    {
        public override TypeNode EvaluateSemantic()
        {
            var type = Operand.EvaluateSemantic();
            if (type != null)
            {
                return type;
            }
            throw new SemanticException($"Cannot and: {(TypeNode) null}", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            throw new NotImplementedException();
        }
    }
}