﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCompi1.Arbol.Expression
{
    public abstract class UnaryOperatorNode: ExpressionNode
    {
        public ExpressionNode Operand { get; set; }
    }
}