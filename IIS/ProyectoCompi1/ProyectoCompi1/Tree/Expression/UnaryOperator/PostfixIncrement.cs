﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    public class PostfixIncrement : UnaryOperatorNode
    {
        public PostfixIncrement(ExpressionNode primaryExp)
        {
            Operand = primaryExp;
        }

        public override TypeNode EvaluateSemantic()
        {
            var idnode = Operand as IdNode;
            if(idnode == null)
                throw new SemanticException("Unary Operand must be and id", CurrentToken);
            if(!SymbolsTable.Instance.VariableExist(idnode.Name))
                throw new SemanticException($"Id {idnode.Name} not exist", CurrentToken);
            var type = Operand.EvaluateSemantic();
            var returnType = Valid(type);
            if (returnType != null)
            {
                return returnType;
            }
            throw new SemanticException($"Cannot postInc: {type}", CurrentToken);
        }

        private TypeNode Valid(TypeNode type)
        {
            if (type is FloatNode)
                return new FloatNode();
            if (type is IntegerNode)
                return new IntegerNode();
            return null;
        }

        public override ValueNode Interpret()
        {
            var leftValue = Operand.Interpret();
            dynamic value = GetValue(leftValue);

            var idnode = Operand as IdNode;
            ValuesTable.Instance.SetVariableValue(idnode.Name, value);
            return value;
        }

        private ValueNode GetValue(ValueNode value)
        {
            var intValue = value as IntegerValue;
            var floatValue = value as FloatValue;

            if (intValue != null)
            {
                return new IntegerValue { Value = intValue.Value + 1};
            }
            return new FloatValue { Value = floatValue.Value + 1};
        }
    }
}