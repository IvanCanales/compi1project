﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class UnaryMulNode : UnaryOperatorNode
    {
        public override TypeNode EvaluateSemantic()
        {
            var type = Operand.EvaluateSemantic();
            if (type != null)
            {
                return type;
            }
            throw new SemanticException($"Cannot mul: {(TypeNode) null}", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            var leftValue = Operand.Interpret();
            var value = GetValue(leftValue);

            var idnode = Operand as IdNode;
            ValuesTable.Instance.SetVariableValue(idnode.Name, value);
            return value;
        }

        private ValueNode GetValue(ValueNode value)
        {
            var intValue = value as IntegerValue;
            var floatValue = value as FloatValue;

            if (intValue != null)
            {
                return new IntegerValue { Value = intValue.Value };
            }
            return new FloatValue { Value = floatValue.Value };
        }
    }
}