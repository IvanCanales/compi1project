﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class UnaryCompNode : UnaryOperatorNode
    {
        public override TypeNode EvaluateSemantic()
        {
            var type = Operand.EvaluateSemantic();
            var returnType = Valid(type);
            if (returnType != null)
            {
                return returnType;
            }
            throw new SemanticException($"Cannot comp: {type}", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            throw new NotImplementedException();
        }

        private TypeNode Valid(TypeNode type)
        {
            if (type is IntegerNode)
                return new IntegerNode();
            return null;
        }

        private ValueNode GetValue(ValueNode value)
        {
            var intValue = value as IntegerValue;
            return new IntegerValue { Value = ~intValue.Value };
        }
    }
}