﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Expression.LinkNode;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    internal class DotLinkNode : LinkNode
    {
        public DotLinkNode(ExpressionNode expression, ExpressionNode accessLink)
        {
            Expression = expression;
            AccessLink = accessLink;
        }

        public override TypeNode EvaluateSemantic()
        {
            var idNode = Expression as IdNode;
            if (idNode != null)
                return ValidateLinkType(idNode, AccessLink);
            throw new SemanticException("Expected id", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            throw new System.NotImplementedException();
        }

        private TypeNode ValidateLinkType(IdNode id, ExpressionNode accessLink)
        {
            if (SymbolsTable.Instance.EnumExist(id.Name))
            {
                return ValidateEnumLink(id, accessLink);
            }
            if(!SymbolsTable.Instance.VariableExist(id.Name))
                throw new SemanticException($"Variable {id.Name} not exist", id.CurrentToken);
            var structNode = SymbolsTable.Instance.GetVariableType(id.Name) as StructNode;
            if(structNode != null)
            {
                if (SymbolsTable.Instance.GetVarLevel(id.Name) != 0)
                    throw new SemanticException($"Cannot access {id.Name}. Try -> ", id.CurrentToken);
                return ValidateStructLink(structNode.Id, accessLink);
            }
            
            throw new SemanticException($"Id {id.Name} cannot have links", id.CurrentToken);
        }

        private TypeNode ValidateEnumLink(IdNode id, ExpressionNode accessLink)
        {
            var enumerator = accessLink as IdNode;
            if (enumerator == null)
                throw new SemanticException("Expected id", accessLink.CurrentToken);
            if (!SymbolsTable.Instance.GetEnum(id.Name).IdExists(enumerator.Name))
                throw new SemanticException("Enumerator not exist", id.CurrentToken);
            return null;
        }

        public override TypeNode EvaluateLink(VarDeclarationStatement variable, int dimension)
        {
            if (variable.Level != 0)
                throw new SemanticException("Cannot access id. Try -> "
                    , CurrentToken);
            var structType = variable.Type as StructNode;
            if(structType != null)
                return ValidateStructLink(structType.Id, AccessLink);
            if (AccessLink == null)
                return variable.Type;
            throw new SemanticException("id cannot be linked", CurrentToken);
        }
    }
}