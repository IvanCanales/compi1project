﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1.Tree.Expression.LinkNode
{
    public abstract class LinkNode: ExpressionNode
    {
        public ExpressionNode Expression;
        public ExpressionNode AccessLink;

        public abstract TypeNode EvaluateLink(VarDeclarationStatement variable, int dimension);

        protected TypeNode ValidateStructLink(IdNode structId, ExpressionNode accessLink)
        {
            var attribute = accessLink as IdNode;
            if (attribute != null)
            {
                if (!SymbolsTable.Instance.StructAttributeExist(structId.Name, attribute.Name))
                    throw new SemanticException($"attribute {attribute.Name} not exist", structId.CurrentToken);
                return SymbolsTable.Instance.GetStructAttribute(structId.Name, attribute.Name).Type;
            }
            else
            {
                var accessAttr = accessLink as LinkNode;
                var accessAttrId = accessAttr.Expression as IdNode;
                if (accessAttrId == null)
                    throw new SemanticException("Expected id", accessAttr.Expression.CurrentToken);
                if (!SymbolsTable.Instance.StructAttributeExist(structId.Name, accessAttrId.Name))
                    throw new SemanticException($"attribute {accessAttrId.Name} not exist"
                        , accessLink.CurrentToken);
                var newStructType = SymbolsTable.Instance.GetStructAttribute(structId.Name, accessAttrId.Name);
                return accessAttr.EvaluateLink(newStructType, 0);
            }
        }
    }
}
