﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.Tree.Expression.LinkNode;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    internal class BracketLinkNode : LinkNode
    {
        public BracketLinkNode(ExpressionNode expression, ExpressionNode accessLink)
        {
            Expression = expression;
            AccessLink = accessLink;
        }

        public override TypeNode EvaluateSemantic()
        {
            var idNode = Expression as IdNode;
            if (idNode != null)
                return ValidateLinkType(idNode);
            throw new SemanticException("Expected id", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            throw new System.NotImplementedException();
        }

        private TypeNode ValidateLinkType(IdNode idNode)
        {
            if(!SymbolsTable.Instance.VariableExist(idNode.Name))
                throw new SemanticException($"Variable: {idNode.Name} not exist", idNode.CurrentToken);
            if (SymbolsTable.Instance.VariableIsArray(idNode.Name, 1) == null)
                throw new SemanticException($"Variable: {idNode.Name} is not an array", idNode.CurrentToken);
            var arrVar = SymbolsTable.Instance.GetVariable(idNode.Name);
            var link = AccessLink as LinkNode;
            if (link != null)
            {
                if(!(link.Expression.EvaluateSemantic() is IntegerNode))
                    throw new SemanticException("Array Pos must be int", link.Expression.CurrentToken);
                return link.EvaluateLink(arrVar, 1);
            }
            else if (AccessLink.EvaluateSemantic() is IntegerNode)
            {
                return arrVar.Type;
            }
            throw new SemanticException("Wrong input for bracket link ", AccessLink.CurrentToken);
        }

        public override TypeNode EvaluateLink(VarDeclarationStatement variable, int dimension)
        {
            dimension++;
            if (!variable.HasDimension(dimension))
                throw new SemanticException($"Variable: {variable.Id.Name} is not an array of dimension {dimension}"
                    , variable.Id.CurrentToken);

            var link = AccessLink as LinkNode;
            if (link != null)
            {
                if (!(link.Expression.EvaluateSemantic() is IntegerNode))
                    throw new SemanticException("Array Pos must be int", link.Expression.CurrentToken);
                link.EvaluateLink(variable, dimension);
            }
            else if (AccessLink.EvaluateSemantic() is IntegerNode)
            {
                return variable.Type;
            }
            throw new SemanticException("Wrong input for bracket link ", AccessLink.CurrentToken);
        }
    }
}