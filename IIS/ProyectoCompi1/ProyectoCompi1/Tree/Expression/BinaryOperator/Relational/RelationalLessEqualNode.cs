﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class RelationalLessEqualNode : BinaryOperatorNode
    {
        public RelationalLessEqualNode(ExpressionNode shiftExpression, ExpressionNode shiftExpression2)
        {
            this.LeftOperand = shiftExpression;
            this.RightOperand = shiftExpression2;
        }

        public override TypeNode EvaluateSemantic()
        {
            CheckLevel();
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();
            var returnValue = Valid(leftType, rightType);
            if (returnValue != null)
            {
                return returnValue;
            }
            throw new SemanticException($"Cannot lessEqu: {leftType} and {rightType}", CurrentToken);
        }

        private TypeNode Valid(TypeNode leftType, TypeNode rightType)
        {
            if ((leftType is IntegerNode && rightType is IntegerNode) ||
            (leftType is IntegerNode && rightType is CharacterNode) ||
            (leftType is IntegerNode && rightType is FloatNode) ||
            (leftType is BooleanNode && rightType is BooleanNode) ||
            (leftType is BooleanNode && rightType is IntegerNode) ||
            (leftType is FloatNode && rightType is FloatNode) ||
            (leftType is FloatNode && rightType is CharacterNode) ||
            (leftType is FloatNode && rightType is IntegerNode) ||
            (leftType is CharacterNode && rightType is CharacterNode) ||
            (leftType is CharacterNode && rightType is IntegerNode) ||
            (leftType is CharacterNode && rightType is FloatNode)) return new BooleanNode();
            return null;
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand.Interpret();
            var rightValue = RightOperand.Interpret();
            return GetValue(leftValue, rightValue);
        }

        private ValueNode GetValue(ValueNode leftValue, ValueNode rightValue)
        {
            var intValueL = leftValue as IntegerValue;
            var floatValueL = leftValue as FloatValue;
            var charValueL = leftValue as CharacterValue;
            var boolValueL = leftValue as BooleanValue;

            var intValueR = rightValue as IntegerValue;
            var floatValueR = rightValue as FloatValue;
            var charValueR = rightValue as CharacterValue;
            var boolValueR = rightValue as BooleanValue;

            if (intValueL != null)
            {
                if (intValueR != null)
                    return new BooleanValue { Value = intValueL.Value <= intValueR.Value };
                if (floatValueR != null)
                    return new BooleanValue { Value = intValueL.Value <= int.Parse(floatValueR.Value.ToString()) };
                if (charValueR != null)
                    return new BooleanValue { Value = intValueL.Value <= charValueR.Value };
            }

            if (boolValueL != null)
            {
                if (boolValueR != null)
                {
                    return new BooleanValue { Value = !(boolValueL.Value && !boolValueR.Value) };
                }
                return new BooleanValue { Value = int.Parse(boolValueL.Value.ToString()) <= intValueR.Value };
            }

            if (floatValueL != null)
            {
                if (floatValueR != null)
                    return new BooleanValue() { Value = floatValueL.Value <= floatValueR.Value };
                if (charValueR != null)
                    return new BooleanValue { Value = floatValueL.Value <= charValueR.Value };
                if (intValueR != null)
                    return new BooleanValue { Value = floatValueL.Value <= intValueR.Value };
            }

            //char left
            if (floatValueR != null)
                return new BooleanValue
                {
                    Value = charValueL.Value <= floatValueR.Value
                };
            if (charValueR != null)
                return new BooleanValue
                {
                    Value = charValueL.Value <= charValueR.Value
                };

            return new BooleanValue
            {
                Value = charValueL.Value <= intValueR.Value
            };
        }
    }
}