﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class SumNode : BinaryOperatorNode
    {
        public SumNode(ExpressionNode multExp, ExpressionNode addExp)
        {
            this.LeftOperand = multExp;
            this.RightOperand = addExp;
        }

        public override TypeNode EvaluateSemantic()
        {
            CheckLevel();
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();
            var returnValue = Valid(leftType, rightType);
            if (returnValue != null)
            {
                return returnValue;
            }
            throw new SemanticException($"Cannot sum: {leftType} and {rightType}", CurrentToken);
        }

        private TypeNode Valid(TypeNode leftType, TypeNode rightType)
        {
            if (leftType is IntegerNode && rightType is IntegerNode) return new IntegerNode();
            if (leftType is IntegerNode && rightType is CharacterNode) return new IntegerNode();
            if (leftType is IntegerNode && rightType is FloatNode) return new IntegerNode();
            if (leftType is BooleanNode && rightType is BooleanNode) return new IntegerNode();
            if (leftType is BooleanNode && rightType is IntegerNode) return new IntegerNode();
            if (leftType is FloatNode && rightType is FloatNode) return new FloatNode();
            if (leftType is FloatNode && rightType is CharacterNode) return new FloatNode();
            if (leftType is FloatNode && rightType is IntegerNode) return new FloatNode();
            if (leftType is CharacterNode && rightType is CharacterNode) return new CharacterNode();
            if (leftType is CharacterNode && rightType is IntegerNode) return new CharacterNode();
            if (leftType is CharacterNode && rightType is FloatNode) return new CharacterNode();
            if (leftType is StringNode && !(rightType is StructNode) && !(rightType is VoidNode)) return new StringNode();
            if (!(leftType is StructNode) && !(leftType is VoidNode) && rightType is StringNode) return new StringNode();
            return null;
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand.Interpret();
            var rightValue = RightOperand.Interpret();
            return GetValue(leftValue, rightValue);
        }

        private ValueNode GetValue(ValueNode leftValue, ValueNode rightValue)
        {
            var intValueL = leftValue as IntegerValue;
            var boolValueL = leftValue as BooleanValue;
            var floatValueL = leftValue as FloatValue;
            var charValueL = leftValue as CharacterValue;
            var stringValueL = leftValue as StringValue;
            var dateValueL = leftValue as DateValue;

            var intValueR = rightValue as IntegerValue;
            var boolValueR = rightValue as BooleanValue;
            var floatValueR = rightValue as FloatValue;
            var charValueR = rightValue as CharacterValue;
            var stringValueR = rightValue as StringValue;
            var dateValueR = rightValue as DateValue;

            if (stringValueR != null)
            {
                if(intValueL != null)
                    return new StringValue {Value = intValueL.Value + stringValueR.Value};
                if (charValueL != null)
                    return new StringValue {Value = charValueL.Value + stringValueR.Value};
                if (floatValueL != null)
                    return new StringValue {Value = floatValueL.Value + stringValueR.Value};
                if (dateValueL != null)
                    return new StringValue {Value = dateValueL.Date + stringValueR.Value};
                if (boolValueL != null)
                    return new StringValue {Value = boolValueL.Value + stringValueR.Value};
                return new StringValue {Value = stringValueL.Value + stringValueR.Value};
            }

            if (stringValueL != null)
            {
                if(intValueR != null)
                    return new StringValue {Value = stringValueL.Value + intValueR.Value};
                if (charValueR != null)
                    return new StringValue {Value = stringValueL.Value + charValueR.Value};
                if (floatValueR != null)
                    return new StringValue {Value = stringValueL.Value + floatValueR.Value};
                if (dateValueR != null)
                    return new StringValue {Value = stringValueL.Value + dateValueR.Date};
                return new StringValue {Value = stringValueL.Value + boolValueR.Value};
            }

            if (intValueL != null)
            {
                if (intValueR != null)
                    return new IntegerValue { Value = intValueL.Value + intValueR.Value };
                if (floatValueR != null)
                    return new IntegerValue { Value = intValueL.Value + int.Parse(floatValueR.Value.ToString()) };
                if (charValueR != null)
                    return new IntegerValue { Value = intValueL.Value + charValueR.Value };
            }

            if (boolValueL != null)
            {
                if (boolValueR != null)
                    return new BooleanValue { Value = boolValueL.Value || boolValueR.Value };
                return new IntegerValue { Value = int.Parse(boolValueL.Value.ToString()) + intValueR.Value };
            }

            if (floatValueL != null)
            {
                if (floatValueR != null)
                    return new FloatValue { Value = floatValueL.Value + floatValueR.Value };
                if (charValueR != null)
                    return new FloatValue { Value = floatValueL.Value + charValueR.Value };
                if (intValueR != null)
                    return new FloatValue { Value = floatValueL.Value + intValueR.Value };
            }

            if (charValueL == null) return null;

            if (floatValueR != null)
                return new CharacterValue
                {
                    Value = char.Parse((charValueL.Value + floatValueR.Value).ToString())
                };
            if (charValueR != null)
                return new CharacterValue
                {
                    Value = char.Parse((charValueL.Value + charValueR.Value).ToString())
                };

            return new CharacterValue
            {
                Value = char.Parse((charValueL.Value + intValueR.Value).ToString())
            };
        }
    }
}