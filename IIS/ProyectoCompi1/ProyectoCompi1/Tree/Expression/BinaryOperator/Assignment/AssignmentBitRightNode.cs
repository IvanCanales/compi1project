﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class AssignmentBitRightNode : AssignmentOperatorNode
    {
        public AssignmentBitRightNode(Token currentToken) : base(currentToken)
        {
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand as IdNode;
            var node = new ShiftRightNode(LeftOperand, RightOperand);
            return ValuesTable.Instance.SetVariableValue(leftValue.Name, node.Interpret());
        }
    }
}