﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class AssignmentMulNode : AssignmentOperatorNode
    {
        public AssignmentMulNode(Token currentToken) : base(currentToken)
        {

        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand as IdNode;
            var node = new MultiplicativeMulNode(LeftOperand, RightOperand);
            return ValuesTable.Instance.SetVariableValue(leftValue.Name, node.Interpret());
        }
    }
}