﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    public class AssignmentOperatorNode : BinaryOperatorNode
    {
        public AssignmentOperatorNode(Token currentToken)
        {
            CurrentToken = currentToken;
        }

        public override TypeNode EvaluateSemantic()
        {
            CheckLevel();
            var leftId = LeftOperand as IdNode;
            if (leftId == null)
                throw new SemanticException($"cannot assign to {LeftOperand.GetType()}", LeftOperand.CurrentToken);
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();
            if (leftType.GetType() == rightType.GetType())
            {
                var leftStruct = leftType as StructNode;
                var rightStruct = rightType as StructNode;
                if(leftStruct != null && leftStruct.Id.Name != rightStruct?.Id.Name)
                    throw new SemanticException("Struct types not equal", RightOperand.CurrentToken);
                return leftType;
            }
            throw new SemanticException($"Cannot assign: {rightType} to {leftType}", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            throw new NotImplementedException();
        }
    }
}