﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class AssignmentBitXorNode : AssignmentOperatorNode
    {
        public AssignmentBitXorNode(Token currentToken) : base(currentToken)
        {
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand as IdNode;
            var node = new ExclusiveOrNode(LeftOperand, RightOperand);
            return ValuesTable.Instance.SetVariableValue(leftValue.Name, node.Interpret());
        }
    }
}