﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class AssignmentSubNode : AssignmentOperatorNode
    {
        public AssignmentSubNode(Token currentToken) : base(currentToken)
        {
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand as IdNode;
            var subNode = new SubNode(LeftOperand, RightOperand);
            return ValuesTable.Instance.SetVariableValue(leftValue.Name, subNode.Interpret());
        }
    }
}