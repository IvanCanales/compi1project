﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class AssignmentSumNode : AssignmentOperatorNode
    {
        public AssignmentSumNode(Token currentToken) : base(currentToken)
        {

        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand as IdNode;
            var node = new SumNode(LeftOperand, RightOperand);
            return ValuesTable.Instance.SetVariableValue(leftValue.Name, node.Interpret());
        }
    }
}