﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class AssignmentEqualityNode : AssignmentOperatorNode
    {
        public AssignmentEqualityNode(Token currentToken) : base(currentToken)
        {
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand as IdNode;
            var rightValue = RightOperand.Interpret();
            return ValuesTable.Instance.SetVariableValue(leftValue.Name, rightValue);
        }
    }
}