﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class AssignmentDivNode : AssignmentOperatorNode
    {
        public AssignmentDivNode(Token currentToken) : base(currentToken)
        {
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand as IdNode;
            var node = new MultiplicativeDivNode(LeftOperand, RightOperand);
            return ValuesTable.Instance.SetVariableValue(leftValue.Name, node.Interpret());
        }
    }
}