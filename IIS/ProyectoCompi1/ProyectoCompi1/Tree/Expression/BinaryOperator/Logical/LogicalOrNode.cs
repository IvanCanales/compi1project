﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1.Arbol.Expression.BinaryOperator
{
    class LogicalOrNode: BinaryOperatorNode
    {
        public LogicalOrNode(ExpressionNode logicalAnd, ExpressionNode logicalAnd2)
        {
            LeftOperand = logicalAnd;
            RightOperand = logicalAnd2;
        }

        public override TypeNode EvaluateSemantic()
        {
            CheckLevel();
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();
            if (Valid(leftType) && Valid(rightType))
                return new BooleanNode();
            throw new SemanticException($"Cannot: {leftType} or {rightType}", CurrentToken);
        }

        private bool Valid(TypeNode node)
        {
            return node is BooleanNode;
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand.Interpret();
            var rightValue = RightOperand.Interpret();
            return GetValue(leftValue, rightValue);
        }

        private ValueNode GetValue(ValueNode leftValue, ValueNode rightValue)
        {
            var boolValueL = leftValue as BooleanValue;
            var boolValueR = rightValue as BooleanValue;

            return new BooleanValue { Value = boolValueL.Value || boolValueR.Value };
        }
    }
}
