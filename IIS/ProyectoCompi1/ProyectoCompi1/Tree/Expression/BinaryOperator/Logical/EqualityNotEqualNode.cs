﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class EqualityNotEqualNode : BinaryOperatorNode
    {
        public EqualityNotEqualNode(ExpressionNode rel, ExpressionNode rel2)
        {
            this.LeftOperand = rel;
            this.RightOperand = rel2;
        }

        public override TypeNode EvaluateSemantic()
        {
            CheckLevel();
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();
            if (Valid(leftType) && Valid(rightType))
                return new BooleanNode();
            throw new SemanticException($"Cannot: {leftType} not equal {rightType}", CurrentToken);
        }

        private bool Valid(TypeNode node)
        {
            return !(node is StructNode) && !(node is DateNode) && !(node is VoidNode);
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand.Interpret();
            var rightValue = RightOperand.Interpret();
            return GetValue(leftValue, rightValue);
        }

        private BooleanValue GetValue(ValueNode leftValue, ValueNode rightValue)
        {
            var intValueL = leftValue as IntegerValue;
            var boolValueL = leftValue as BooleanValue;
            var floatValueL = leftValue as FloatValue;
            var charValueL = leftValue as CharacterValue;
            var stringValueL = leftValue as StringValue;
            var dateValueL = leftValue as DateValue;

            var intValueR = rightValue as IntegerValue;
            var boolValueR = rightValue as BooleanValue;
            var floatValueR = rightValue as FloatValue;
            var charValueR = rightValue as CharacterValue;
            var stringValueR = rightValue as StringValue;
            var dateValueR = rightValue as DateValue;

            if (stringValueR != null)
            {
                if (intValueL != null)
                    return new BooleanValue { Value = intValueL.Value.ToString() != stringValueR.Value };
                if (charValueL != null)
                    return new BooleanValue { Value = charValueL.Value.ToString() != stringValueR.Value };
                if (floatValueL != null)
                    return new BooleanValue { Value = floatValueL.Value.ToString() != stringValueR.Value };
                if (dateValueL != null)
                    return new BooleanValue { Value = dateValueL.Date != stringValueR.Value };
                if (boolValueL != null)
                    return new BooleanValue { Value = boolValueL.Value.ToString() != stringValueR.Value };
                return new BooleanValue { Value = stringValueL.Value != stringValueR.Value };
            }

            if (stringValueL != null)
            {
                if (intValueR != null)
                    return new BooleanValue { Value = intValueR.Value.ToString() != stringValueL.Value };
                if (charValueR != null)
                    return new BooleanValue { Value = charValueR.Value.ToString() != stringValueL.Value };
                if (floatValueR != null)
                    return new BooleanValue { Value = floatValueR.Value.ToString() != stringValueL.Value };
                if (dateValueR != null)
                    return new BooleanValue { Value = dateValueR.Date != stringValueL.Value };
                return new BooleanValue { Value = boolValueR.Value.ToString() != stringValueL.Value };
            }

            if (intValueL != null)
            {
                if (intValueR != null)
                    return new BooleanValue { Value = intValueL.Value != intValueR.Value };
                if (floatValueR != null)
                    return new BooleanValue { Value = intValueL.Value != int.Parse(floatValueR.Value.ToString()) };
                if (charValueR != null)
                    return new BooleanValue { Value = intValueL.Value != charValueR.Value };
            }

            if (boolValueL != null)
            {
                if (boolValueR != null)
                    return new BooleanValue { Value = boolValueL.Value != boolValueR.Value };
                return new BooleanValue { Value = int.Parse(boolValueL.Value.ToString()) != intValueR.Value };
            }

            if (floatValueL != null)
            {
                if (floatValueR != null)
                    return new BooleanValue() { Value = floatValueL.Value != floatValueR.Value };
                if (charValueR != null)
                    return new BooleanValue { Value = floatValueL.Value != charValueR.Value };
                if (intValueR != null)
                    return new BooleanValue { Value = floatValueL.Value != intValueR.Value };
            }

            //char left
            if (floatValueR != null)
                return new BooleanValue
                {
                    Value = charValueL.Value != floatValueR.Value
                };
            if (charValueR != null)
                return new BooleanValue
                {
                    Value = charValueL.Value != charValueR.Value
                };

            return new BooleanValue
            {
                Value = charValueL.Value != intValueR.Value
            };
        }
    }
}