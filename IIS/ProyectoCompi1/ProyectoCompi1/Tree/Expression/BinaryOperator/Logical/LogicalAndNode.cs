﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    public class LogicalAndNode : BinaryOperatorNode
    {
        public LogicalAndNode(ExpressionNode inclusiveOr, ExpressionNode inclusiveOr2)
        {
            this.LeftOperand = inclusiveOr;
            this.RightOperand = inclusiveOr2;
        }

        public override TypeNode EvaluateSemantic()
        {
            CheckLevel();
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();
            if (Valid(leftType) && Valid(rightType))
                return new BooleanNode();
            throw new SemanticException($"Cannot: {leftType} and {rightType}", CurrentToken);
        }

        private bool Valid(TypeNode node)
        {
            return !(node is StructNode) && !(node is VoidNode) && !(node is DateNode);
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand.Interpret();
            var rightValue = RightOperand.Interpret();
            return GetValue(leftValue, rightValue);
        }

        private ValueNode GetValue(ValueNode leftValue, ValueNode rightValue)
        {
            var boolValueL = leftValue as BooleanValue;
            var boolValueR = rightValue as BooleanValue;

            return new BooleanValue { Value = boolValueL.Value || boolValueR.Value };
        }
    }
}