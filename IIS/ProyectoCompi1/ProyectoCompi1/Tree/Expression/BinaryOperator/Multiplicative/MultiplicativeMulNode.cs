﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class MultiplicativeMulNode : BinaryOperatorNode
    {
        public MultiplicativeMulNode(ExpressionNode unary, ExpressionNode multExp)
        {
            this.LeftOperand = unary;
            this.RightOperand = multExp;
        }

        public override TypeNode EvaluateSemantic()
        {
            CheckLevel();
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();
            var returnValue = Valid(leftType, rightType);
            if (returnValue != null)
            {
                return returnValue;
            }
            throw new SemanticException($"Cannot mul: {leftType} and {rightType}", CurrentToken);
        }

        private TypeNode Valid(TypeNode leftType, TypeNode rightType)
        {
            if (leftType is IntegerNode && rightType is IntegerNode) return new IntegerNode();
            if (leftType is IntegerNode && rightType is CharacterNode) return new IntegerNode();
            if (leftType is IntegerNode && rightType is FloatNode) return new IntegerNode();
            if (leftType is BooleanNode && rightType is BooleanNode) return new IntegerNode();
            if (leftType is BooleanNode && rightType is IntegerNode) return new IntegerNode();
            if (leftType is FloatNode && rightType is FloatNode) return new FloatNode();
            if (leftType is FloatNode && rightType is CharacterNode) return new FloatNode();
            if (leftType is FloatNode && rightType is IntegerNode) return new FloatNode();
            if (leftType is CharacterNode && rightType is CharacterNode) return new CharacterNode();
            if (leftType is CharacterNode && rightType is IntegerNode) return new CharacterNode();
            if (leftType is CharacterNode && rightType is FloatNode) return new CharacterNode();
            return null;
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand.Interpret();
            var rightValue = RightOperand.Interpret();
            return GetValue(leftValue, rightValue);
        }

        private ValueNode GetValue(ValueNode leftValue, ValueNode rightValue)
        {
            var intValueL = leftValue as IntegerValue;
            var boolValueL = leftValue as BooleanValue;
            var floatValueL = leftValue as FloatValue;
            var charValueL = leftValue as CharacterValue;

            var intValueR = rightValue as IntegerValue;
            var boolValueR = rightValue as BooleanValue;
            var floatValueR = rightValue as FloatValue;
            var charValueR = rightValue as CharacterValue;

            if (intValueL != null)
            {
                if (intValueR != null)
                    return new IntegerValue { Value = intValueL.Value * intValueR.Value };
                if (floatValueR != null)
                    return new IntegerValue { Value = intValueL.Value * int.Parse(floatValueR.Value.ToString()) };
                if (charValueR != null)
                    return new IntegerValue { Value = intValueL.Value * charValueR.Value };
            }

            if (boolValueL != null)
            {
                if (boolValueR != null)
                    return new BooleanValue { Value = boolValueL.Value && boolValueR.Value };
                return new IntegerValue { Value = int.Parse(boolValueL.Value.ToString()) * intValueR.Value };
            }

            if (floatValueL != null)
            {
                if (floatValueR != null)
                    return new FloatValue { Value = floatValueL.Value * floatValueR.Value };
                if (charValueR != null)
                    return new FloatValue { Value = floatValueL.Value * charValueR.Value };
                if (intValueR != null)
                    return new FloatValue { Value = floatValueL.Value * intValueR.Value };
            }

            if (charValueL == null) return null;

            if (floatValueR != null)
                return new CharacterValue
                {
                    Value = char.Parse((charValueL.Value * floatValueR.Value).ToString())
                };
            if (charValueR != null)
                return new CharacterValue
                {
                    Value = char.Parse((charValueL.Value * charValueR.Value).ToString())
                };

            return new CharacterValue
            {
                Value = char.Parse((charValueL.Value * intValueR.Value).ToString())
            };
        }
    }
}