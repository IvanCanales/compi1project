﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class InclusiveOrNode : BinaryOperatorNode
    {
        public InclusiveOrNode(ExpressionNode exclusiveOr, ExpressionNode exclusiveOr2)
        {
            this.LeftOperand = exclusiveOr;
            this.RightOperand = exclusiveOr2;
        }

        public override TypeNode EvaluateSemantic()
        {
            CheckLevel();
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();
            if (Valid(leftType) && Valid(rightType))
                return leftType;
            throw new SemanticException($"Cannot inclusive or: {leftType} and {rightType}", CurrentToken);
        }

        public override ValueNode Interpret()
        {
            var leftValue = LeftOperand.Interpret();
            var rightValue = RightOperand.Interpret();
            return GetValue(leftValue, rightValue);
        }

        private bool Valid(TypeNode node)
        {
            return node is IntegerNode || node is CharacterNode;
        }

        private ValueNode GetValue(ValueNode leftValue, ValueNode rightValue)
        {
            var intValueL = leftValue as IntegerValue;
            var charValueL = leftValue as CharacterValue;

            var intValueR = rightValue as IntegerValue;
            var charValueR = rightValue as CharacterValue;

            if (intValueL != null)
            {
                if (intValueR != null)
                    return new IntegerValue { Value = intValueL.Value | intValueR.Value };
                if (charValueR != null)
                    return new IntegerValue { Value = intValueL.Value | charValueR.Value };
            }

            if (charValueR != null)
                return new CharacterValue
                {
                    Value = char.Parse((charValueL.Value | charValueR.Value).ToString())
                };

            return new CharacterValue
            {
                Value = char.Parse((charValueL.Value | intValueR.Value).ToString())
            };
        }
    }
}