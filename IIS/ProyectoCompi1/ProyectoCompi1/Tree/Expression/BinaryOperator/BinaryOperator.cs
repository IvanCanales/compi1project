﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1.Arbol.Expression
{
    public abstract class BinaryOperatorNode: ExpressionNode
    {
        public ExpressionNode LeftOperand { get; set; }
        public ExpressionNode RightOperand { get; set; }

        public void CheckLevel()
        {
            SymbolsTable.CompareLevel(LeftOperand, RightOperand, CurrentToken);
        }
    }
}
