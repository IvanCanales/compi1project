﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class LiteralDateNode : LiteralNode
    {
        public LiteralDateNode(string literal, Token currentToken) : base(currentToken)
        {
            this.CurrentToken = currentToken;
        }

        public override TypeNode EvaluateSemantic()
        {
            return new DateNode();
        }

        public override ValueNode Interpret()
        {
            return new DateValue(Literal);
        }
    }
}