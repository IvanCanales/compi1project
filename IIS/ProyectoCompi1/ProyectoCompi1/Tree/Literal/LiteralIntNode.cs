﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class LiteralIntNode : LiteralNode
    {
        public LiteralIntNode(string literal, Token currentToken): base(currentToken)
        {
            this.Literal = literal;
        }

        public override TypeNode EvaluateSemantic()
        {
            return new IntegerNode();
        }

        public override ValueNode Interpret()
        {
            return new IntegerValue {Value = int.Parse(Literal)};
        }
    }
}