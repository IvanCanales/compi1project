﻿using ProyectoCompi1.Arbol.Expression;

namespace ProyectoCompi1
{
    public abstract class LiteralNode : ExpressionNode
    {
        public string Literal;

        protected LiteralNode(Token currentToken)
        {
            CurrentToken = currentToken;
        }
    }
}