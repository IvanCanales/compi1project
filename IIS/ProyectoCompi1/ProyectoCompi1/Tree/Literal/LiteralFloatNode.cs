﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class LiteralFloatNode : LiteralNode
    {
        public LiteralFloatNode(string literal, Token currentToken) : base(currentToken)
        {
            this.Literal = literal;
        }

        public override TypeNode EvaluateSemantic()
        {
            return new FloatNode();
        }

        public override ValueNode Interpret()
        {
            return new FloatValue {Value = float.Parse(Literal)};
        }
    }
}