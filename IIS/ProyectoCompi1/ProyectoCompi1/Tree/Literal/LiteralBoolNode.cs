﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1.Tree.Literal
{
    class LiteralBoolNode : LiteralNode
    {
        public LiteralBoolNode(string literal, Token currentToken) : base(currentToken)
        {
            this.Literal = literal;
        }

        public override TypeNode EvaluateSemantic()
        {
            return new BooleanNode();
        }

        public override ValueNode Interpret()
        {
            return new BooleanValue {Value = Literal == "true"};
        }
    }
}
