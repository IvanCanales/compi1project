﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class LiteralCharNode : LiteralNode
    {
        public LiteralCharNode(string literal, Token currentToken) : base(currentToken)
        {
            this.Literal = literal;
        }

        public override TypeNode EvaluateSemantic()
        {
            return new CharacterNode();
        }

        public override ValueNode Interpret()
        {
            return new CharacterValue { Value = Literal[0]};
        }
    }
}