﻿using System.Collections.Generic;
using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class LiteralStringNode : LiteralNode
    {
        public LiteralStringNode(string literal, Token currentToken) : base(currentToken)
        {
            this.Literal = literal;
        }

        public override TypeNode EvaluateSemantic()
        {
            return new StringNode();
        }

        public override ValueNode Interpret()
        {
            return new StringValue {Value = Literal};
        }
    }
}