﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;

namespace ProyectoCompi1.TypeSpecifiers
{
    public class SymbolsTable
    {
        private static SymbolsTable _instance;
        private List<Dictionary<string, VarDeclarationStatement>> _variables;
        private Dictionary<string, List<VarDeclarationStatement>> _structs;
        private Dictionary<string, Enumerator> _enums;
        private Dictionary<string, Tuple<TypeNode, Param>> _functions;

        private SymbolsTable()
        {
            _variables = new List<Dictionary<string, VarDeclarationStatement>>();
            _functions = new Dictionary<string, Tuple<TypeNode, Param>>();
            _structs = new Dictionary<string, List<VarDeclarationStatement>>();
            _enums = new Dictionary<string, Enumerator>();

            _variables.Add(new Dictionary<string, VarDeclarationStatement>());
        }

        public void Destroyer()
        {
            _instance = new SymbolsTable();
        }

        public static SymbolsTable Instance
        {
            get
            {
                if (_instance != null) return _instance;
                _instance = new SymbolsTable();
                return _instance;
            }
        }

        public void DeclareVariable(string name, VarDeclarationStatement variable)
        {
            _variables.Last().Add(name, variable);
        }

        public TypeNode GetVariableType(string name)
        {
            for (var i = _variables.Count-1; i >= 0; i--)
            {
                TypeNode currentVar = null;
                if(_variables[i].ContainsKey(name))
                    currentVar = _variables[i][name].Type;
                if (currentVar != null)
                    return currentVar;
            }
            return null;
        }

        public VarDeclarationStatement GetVariable(string name)
        {
            for (var i = _variables.Count-1; i >= 0; i--)
            {
                VarDeclarationStatement currentVar = null;
                if(_variables[i].ContainsKey(name))
                    currentVar = _variables[i][name];
                if (currentVar != null)
                    return currentVar;
            }
            return null;
        }

        public int GetVarLevel(string name)
        {
            for (var i = _variables.Count - 1; i >= 0; i--)
            {
                TypeNode currentVar = null;
                var currentLevel = 0;
                if (_variables[i].ContainsKey(name))
                {
                    currentVar = _variables[i][name].Type;
                    currentLevel = _variables[i][name].Level;
                }
                if (currentVar != null)
                    return currentLevel;
            }
            return -1;
        }

        public bool VariableExistLastContext(string name)
        {
            return _variables.Last().ContainsKey(name);
        }

        public bool VariableExist(string name)
        {
            for (var i = _variables.Count - 1; i >= 0; i--)
            {
                TypeNode currentVar = null;
                if (_variables[i].ContainsKey(name))
                    currentVar = _variables[i][name].Type;
                if (currentVar != null)
                    return true;
            }
            return false;
        }

        public TypeNode VariableIsArray(string name, int dimension)
        {
            for (var i = _variables.Count - 1; i >= 0; i--)
            {
                if (!_variables[i].ContainsKey(name)) continue;
                var currentVar = _variables[i][name];
                return currentVar.HasDimension(dimension) ? currentVar.Type : null;
            }
            return null;
        }

        public bool FunctionExist(string name)
        {
            return _functions.ContainsKey(name);
        }

        public void DeclareFunction(string name, Tuple<TypeNode, Param> param)
        {
            _functions.Add(name, param);
        }

        public TypeNode GetFunction(string name, ExpressionNode param, Token currentToken)
        {
            if (_functions.ContainsKey(name))
            {
                var function = _functions[name];
                if (function.Item2 == null && param == null)
                    return function.Item1;
                if(function.Item2 != null && param != null && function.Item2.CheckFunCall(param))
                        return function.Item1;
                throw new SemanticException($"cannot call function {name} with these params", currentToken);
            }
            throw new SemanticException($"function {name} does not exist", currentToken);
        }

        public bool StructExist(string name)
        {
            return _structs.ContainsKey(name);
        }

        public bool StructAttributeExist(string structName, string attributeName)
        {
            var structAttr = _structs[structName];
            return structAttr.Any(variable => variable.Id.Name == attributeName);
        }

        public VarDeclarationStatement GetStructAttribute(string structName, string attributeName)
        {
            var structAttr = _structs[structName];
            foreach (var attr in structAttr)
            {
                if (attr.Id.Name == attributeName)
                    return attr;
            }
            return null;
        }

        public void DeclareStruct(string name, List<VarDeclarationStatement> attributes)
        {
            _structs.Add(name, attributes);
        }

        public bool EnumExist(string name)
        {
            return _enums.ContainsKey(name);
        }

        public void DeclareEnum(string name, Enumerator enums)
        {
            _enums.Add(name, enums);
        }

        public Enumerator GetEnum(string name)
        {
            return _enums[name];
        }

        public void IncrementStack()
        {
            _variables.Add(new Dictionary<string, VarDeclarationStatement>());
        }

        public void DecrementStack()
        {
            _variables.Remove(_variables.Last());
        }

        public static void CompareLevel(ExpressionNode leftOperand, ExpressionNode rightOperand, Token errorToken)
        {
            var leftId = leftOperand as IdNode;
            var rightId = rightOperand as IdNode;
            if (leftId != null && rightId != null)
            {
                var leftlevel = Instance.GetVarLevel(leftId.Name);
                var rightlevel = Instance.GetVarLevel(rightId.Name);
                if (leftlevel != rightlevel)
                    throw new SemanticException($"cannot assign {leftlevel} Indirection and " +
                                                $"{rightlevel} Indirection", errorToken);
            }
        }
    }
}
