﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class StructNode : TypeNode
    {
        public IdNode Id;

        public StructNode(IdNode id)
        {
            this.Id = id;
        }

        public StructNode()
        {
            
        }

        public override ValueNode GetDefaultValue()
        {
            return null;
        }
    }
}