﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class VoidNode : TypeNode
    {
        public override ValueNode GetDefaultValue()
        {
            return null;
        }
    }
}