﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class CaseNode: TypeNode
    {
        public TypeNode CaseType;

        public CaseNode(TypeNode caseType)
        {
            this.CaseType = caseType;
        }

        public override ValueNode GetDefaultValue()
        {
            return CaseType.GetDefaultValue();
        }
    }
}