﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1.TypeSpecifiers
{
    public class ReturnNode: TypeNode
    {
        public TypeNode ReturnType;

        public ReturnNode()
        {
        }

        public ReturnNode(TypeNode returnType)
        {
            this.ReturnType = returnType;
        }

        public override ValueNode GetDefaultValue()
        {
            return ReturnType.GetDefaultValue();
        }
    }
}
