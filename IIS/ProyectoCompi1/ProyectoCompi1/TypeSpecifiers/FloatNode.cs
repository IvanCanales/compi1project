﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class FloatNode : TypeNode
    {
        public override ValueNode GetDefaultValue()
        {
            return new FloatValue {Value = 0.0f};
        }
    }
}