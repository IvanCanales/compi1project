﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public abstract class TypeNode
    {
        public abstract ValueNode GetDefaultValue();
    }
}