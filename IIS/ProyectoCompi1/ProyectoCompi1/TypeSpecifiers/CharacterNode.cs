﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class CharacterNode : TypeNode
    {
        public override ValueNode GetDefaultValue()
        {
            return new CharacterValue {Value = ' '};
        }
    }
}