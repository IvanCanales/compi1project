﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    public class DateNode : TypeNode
    {
        public override ValueNode GetDefaultValue()
        {
            return new DateValue{Date = "666", Day = "6", Month = "6", Year = "6"};
        }
    }
}