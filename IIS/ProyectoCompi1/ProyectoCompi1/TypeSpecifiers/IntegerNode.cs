﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class IntegerNode : TypeNode
    {
        public override ValueNode GetDefaultValue()
        {
            return new IntegerValue {Value = 0};
        }
    }
}