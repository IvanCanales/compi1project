﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class BooleanNode : TypeNode
    {
        public override ValueNode GetDefaultValue()
        {
            return new BooleanValue {Value = false};
        }
    }
}