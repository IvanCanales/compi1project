﻿using ProyectoCompi1.Interpret;

namespace ProyectoCompi1
{
    internal class StringNode : TypeNode
    {
        public override ValueNode GetDefaultValue()
        {
            return new StringValue {Value = ""};
        }
    }
}