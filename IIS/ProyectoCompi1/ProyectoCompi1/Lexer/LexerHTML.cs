﻿namespace ProyectoCompi1.Lexer
{
    public partial class Lexer
    {
        private Token ConsumeHtml(char currenChar)
        {
            if (currenChar == '<')
            {
                var lexeme = currenChar.ToString();
                CurrentPointer++;
                _currentCol++;
                currenChar = GetCurrentSymbol();
                if (currenChar == '%')
                {
                    CurrentPointer++;
                    _currentCol++;
                    _htmlMode = false;
                    return GetNextToken();
                }
                while (currenChar != '>')
                {
                    lexeme += currenChar;
                    CurrentPointer++;
                    _currentCol++;
                    currenChar = GetCurrentSymbol();
                }
                lexeme += currenChar;
                CurrentPointer++;
                _currentCol++;
                return SetTokenLineCol(new Token {Type = TokenTypes.Html, Lexeme = lexeme});
            }
            throw new LexerException("Expected <");
        }
    }
}