﻿using System;

namespace ProyectoCompi1.Lexer
{
    public partial class Lexer
    {
        public Token GetDigit(string lexeme)
        {
            var tmp1 = GetCurrentSymbol();
            while (Char.IsDigit(tmp1))
            {
                lexeme += tmp1;
                CurrentPointer++;
                _currentCol++;
                tmp1 = GetCurrentSymbol();
            }

            var numToken = IsHexadecimal(lexeme);
            if (numToken != null)
                return numToken;
            if (IsOctal(lexeme))
                return new Token { Type = TokenTypes.LitOctal, Lexeme = lexeme };
            if (IsDecimal(lexeme))
                return new Token { Type = TokenTypes.LitDecimal, Lexeme = lexeme };
            numToken = IsFloat(lexeme);
            if (numToken != null)
                return numToken;
            else
                throw new LexerException("not a number");
        }

        private Token IsHexadecimal(string lexeme)
        {
            var tmp1 = GetCurrentSymbol();
            if (!(lexeme == "0" && tmp1 == 'x'))
                return null;
            while (Char.IsLetterOrDigit(tmp1))
            {
                lexeme += tmp1;
                CurrentPointer++;
                _currentCol++;
                tmp1 = GetCurrentSymbol();
            }
            return System.Text.RegularExpressions.Regex.IsMatch(lexeme, @"\A\b(0[xX])?[0-9a-fA-F]+\b\Z") ?
                new Token { Type = TokenTypes.LitHex, Lexeme = lexeme } : null;
        }

        private static bool IsOctal(string lexeme)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(lexeme, "^0[1-7][0-7]+$");
        }

        private bool IsDecimal(string lexeme)
        {
            if (GetCurrentSymbol() == '.' || GetCurrentSymbol() == 'e')
                return false;
            int result;
            return int.TryParse(lexeme, out result);
        }

        private Token IsFloat(string lexeme)
        {
            var nextSymbol = GetCurrentSymbol();
            if (nextSymbol != '.' && nextSymbol != 'e') return null;
            do
            {
                lexeme += nextSymbol;
                CurrentPointer++;
                _currentCol++;
                nextSymbol = GetCurrentSymbol();
            } while (Char.IsDigit(nextSymbol));
            if (nextSymbol != 'f')
                throw new LexerException("Mising f in float literal");
            lexeme += nextSymbol;
            CurrentPointer++;
            _currentCol++;
            return new Token { Type = TokenTypes.LitFloat, Lexeme = lexeme };
        }
    }
}