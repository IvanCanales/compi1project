﻿using System.Collections.Generic;

namespace ProyectoCompi1.Lexer
{
    public partial class Lexer
    {
        public static string SourceCode;
        public static int CurrentPointer;
        private readonly Dictionary<string, TokenTypes> _reserveWords;
        private readonly Dictionary<string, TokenTypes> _symbols;
        private int _currentLine;
        private int _currentCol;
        private bool _htmlMode;

        public Lexer(string sourceCode)
        {
            SourceCode = sourceCode;
            CurrentPointer = 0;
            _currentLine = 1;
            _currentCol = 1;
            _htmlMode = true;

            _reserveWords = new Dictionary<string, TokenTypes>
            {
                {"int", TokenTypes.DataInteger},
                {"float", TokenTypes.DataFloat},
                {"char", TokenTypes.DataCharacter},
                {"bool", TokenTypes.DataBoolean},
                {"string", TokenTypes.DataString},
                {"date", TokenTypes.DataDate},
                {"void", TokenTypes.DataVoid},
                {"const", TokenTypes.Constant},
                {"true", TokenTypes.LitTrue},
                {"false", TokenTypes.LitFalse},
                {"if", TokenTypes.If},
                {"else", TokenTypes.Else},
                {"while", TokenTypes.LoopWhile},
                {"do", TokenTypes.LoopDoWhile},
                {"for", TokenTypes.LoopFor},
                {"switch", TokenTypes.Switch},
                {"case", TokenTypes.SwitchCase},
                {"default", TokenTypes.SwitchDefault},
                {"break", TokenTypes.Break},
                {"continue", TokenTypes.Continue},
                {"enum", TokenTypes.Enum},
                {"struct", TokenTypes.Struct},
                {"return", TokenTypes.Return},
                {"print", TokenTypes.Print},
                {"GetQueryString", TokenTypes.GetQuery},
                {"GetForm", TokenTypes.GetForm},
                {"GetMethodFunc", TokenTypes.GetMethod},
                {"atoi", TokenTypes.Atoi}
            };

            _symbols = new Dictionary<string, TokenTypes>
            {
                {"+", TokenTypes.OpSum},
                {"-", TokenTypes.OpSub},
                {"*", TokenTypes.OpMul},
                {"/", TokenTypes.OpDiv},
                {"=", TokenTypes.OpAssigEqu},
                {";", TokenTypes.EndStm},
                {":", TokenTypes.Colon},
                {".", TokenTypes.DeRefDot},
                {"(", TokenTypes.OpenParenthesis},
                {")", TokenTypes.CloseParenthesis},
                {"{", TokenTypes.OpenBrackets},
                {"}", TokenTypes.CloseBrackets},
                {"\"", TokenTypes.LitString},
                {"\'", TokenTypes.LitChar},
                {"%", TokenTypes.OpMod},
                {"+=", TokenTypes.OpAssigSum},
                {"-=", TokenTypes.OpAssigSub},
                {"*=", TokenTypes.OpAssigMul},
                {"/=", TokenTypes.OpAssigDiv},
                {"%=", TokenTypes.OpAssigMod},
                {"==", TokenTypes.OpRelEqu},
                {"!=", TokenTypes.OpRelNequ},
                {">", TokenTypes.OpRelGtr},
                {"<", TokenTypes.OpRelLs},
                {">=", TokenTypes.OpRelGtrEqu},
                {"<=", TokenTypes.OpRelLsEqu},
                {"&=", TokenTypes.OpAssigBitAnd},
                {"^=", TokenTypes.OpAssigBitXor},
                {"|=", TokenTypes.OpAssigBitOr},
                {"<<=", TokenTypes.OpAssigBitL},
                {">>=", TokenTypes.OpAssigBitR},
                {"&&", TokenTypes.OpLogAnd},
                {"||", TokenTypes.OpLogOr},
                {"!", TokenTypes.OpLogNot},
                {"<<", TokenTypes.OpBitL},
                {">>", TokenTypes.OpBitR},
                {"|", TokenTypes.OpBitOr},
                {"&", TokenTypes.OpBitAnd},
                {"~", TokenTypes.OpBitComp},
                {"^", TokenTypes.OpBitXor},
                {"->", TokenTypes.DeRefArrow},
                {"++", TokenTypes.OpInc},
                {"--", TokenTypes.OpDec},
                {"[", TokenTypes.OpenArr},
                {"]", TokenTypes.CloseArr},
                {"#", TokenTypes.Hash},
                {",", TokenTypes.Comma},
                {"?", TokenTypes.OpInterrogation}
            };
        }

        private Token SetTokenLineCol(Token token)
        {
            if (token == null) return null;
            token.Col = _currentCol - token.Lexeme.Length;
            token.Line = _currentLine;
            return token;
        }
    }
}