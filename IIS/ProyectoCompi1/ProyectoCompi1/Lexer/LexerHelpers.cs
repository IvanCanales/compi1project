﻿using System;

namespace ProyectoCompi1.Lexer
{
    //Todo float first letter is e in hex
    public partial class Lexer
    {
        private Token GetHashLexeme()
        {
            var nextSymbol = GetCurrentSymbol();
            if (nextSymbol == 'i')
                return GetInclude();
            else if (Char.IsDigit(nextSymbol))
                return GetDate();
            throw new LexerException("Wrong # token.");
        }

        private Token GetDate()
        {
            var lexeme = "";
            for(var i=0; i<11; i++)
            {
                lexeme += GetCurrentSymbol();
                CurrentPointer++;
                _currentCol++;
            }
            var day = lexeme[1].ToString() + lexeme[2];
            var month = lexeme[4].ToString() + lexeme[5];
            var year = lexeme[7].ToString() + lexeme[8] + lexeme[9] + lexeme[10];

            try
            {
                var dayNum = int.Parse(day);
                var monthNum = int.Parse(month);
                var yearNum = int.Parse(year);
                if (dayNum >= 1 && dayNum <= 31 && monthNum >= 1 && monthNum <= 12 && yearNum > 0 && lexeme[3] == '-' && lexeme[6] == '-' && lexeme[11] == '#')
                    return new Token { Type = TokenTypes.LitDate, Lexeme = lexeme.TrimEnd('#') };
            }
            catch (Exception)
            {
                throw new LexerException("Wrong date format.");
            }
            throw new LexerException("Wrong date format.");
        }

        private Token GetInclude()
        {
            var lexeme = "";
            for(var i=0; i < 7; i++)
            {
                lexeme += GetCurrentSymbol();
                CurrentPointer++;
                _currentCol++;
            }
            if (lexeme == "include")
                return new Token { Type = TokenTypes.LibInclude, Lexeme = lexeme };
            throw new LexerException("wrong include format.");
        }

        private Token GetChar()
        {
            var lexeme = "";
            var currentSym = GetCurrentSymbol();
            if (currentSym == '\\')
            {
                lexeme += currentSym;
                CurrentPointer++;
                _currentCol++;
                currentSym = GetCurrentSymbol();
            }
            lexeme += currentSym;
            CurrentPointer++;
            _currentCol++;
            currentSym = GetCurrentSymbol();

            if (currentSym != '\'')
                throw new LexerException("Missing char close statment (').");
            CurrentPointer++;
            _currentCol++;
            return new Token { Type = TokenTypes.LitChar, Lexeme = lexeme };
        }

        private Token GetString()
        {
            var previosSym = "";
            var lexeme = previosSym;
            var currentSym = GetCurrentSymbol();
            while (currentSym != '\"' || (currentSym == '\"' && previosSym == @"\"))
            {
                if (currentSym == '\0')
                    throw new LexerException("Missing string close statment (\").");
                lexeme += currentSym;
                CurrentPointer++;
                _currentCol++;
                previosSym = currentSym.ToString();
                currentSym = GetCurrentSymbol();
            }

            lexeme += "";
            CurrentPointer++;
            _currentCol++;
            return new Token { Type = TokenTypes.LitString, Lexeme = lexeme };
        }

        private Token GetId(string lexeme)
        {
            var tmp1 = GetCurrentSymbol();
            while (Char.IsLetterOrDigit(tmp1) || tmp1 == '_' || Char.IsDigit(tmp1))
            {
                lexeme += tmp1;
                CurrentPointer++;
                _currentCol++;
                tmp1 = GetCurrentSymbol();
            }
            return new Token { Type = _reserveWords.ContainsKey(lexeme) ? _reserveWords[lexeme] : TokenTypes.Id, Lexeme = lexeme };
        }

        public char GetCurrentSymbol()
        {
            var nextSymbol = '\0';
            if (CurrentPointer < SourceCode.Length)
                nextSymbol = SourceCode[CurrentPointer];
            return nextSymbol;
        }
    }
}