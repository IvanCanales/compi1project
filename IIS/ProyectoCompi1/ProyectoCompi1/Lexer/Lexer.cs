﻿using System;

namespace ProyectoCompi1.Lexer
{
    public partial class Lexer
    {
        public Token GetNextToken()
        {
            while (true)
            {
                Token token;
                var currentChar = GetCurrentSymbol();
                var lexeme = "";

                while (Char.IsWhiteSpace(currentChar))
                {
                    CurrentPointer++;
                    switch (currentChar)
                    {
                        case '\n':
                            _currentLine++;
                            _currentCol = 1;
                            break;
                        case '\t':
                            _currentCol += 4;
                            break;
                        default:
                            _currentCol++;
                            break;
                    }
                    currentChar = GetCurrentSymbol();
                }

                if (currentChar == '\0')
                {
                    return new Token {Type = TokenTypes.Eof};
                }

                if (_htmlMode)
                {
                    return ConsumeHtml(currentChar);
                }

                if (char.IsLetter(currentChar) || currentChar == '_')
                {
                    lexeme += currentChar;
                    CurrentPointer++;
                    _currentCol++;
                    token = GetId(lexeme);
                    return SetTokenLineCol(token);
                }

                if (char.IsDigit(currentChar))
                {
                    lexeme += currentChar;
                    CurrentPointer++;
                    _currentCol++;
                    token = GetDigit(lexeme);
                    return SetTokenLineCol(token);
                }

                if (!_symbols.ContainsKey(currentChar.ToString()))
                    throw new LexerException("Token not Found");
                CurrentPointer++;
                _currentCol++;
                if (currentChar == '/')
                {
                    if (GetCurrentSymbol() == '/' || GetCurrentSymbol() == '*')
                    {
                        GetComment();
                        var tok = GetNextToken();
                        return tok;
                    }
                }
                if (currentChar == '\"')
                {
                    token = GetString();
                    return SetTokenLineCol(token);
                }
                else if (currentChar == '\'')
                {
                    token = GetChar();
                    return SetTokenLineCol(token);
                }
                else if (currentChar == '#')
                {
                    token = GetHashLexeme();
                    return SetTokenLineCol(token);
                }
                var multipleSymbol = currentChar + GetCurrentSymbol().ToString();
                CurrentPointer++;
                _currentCol++;
                multipleSymbol += GetCurrentSymbol();
                if (_symbols.ContainsKey(multipleSymbol))
                {
                    CurrentPointer++;
                    _currentCol++;
                    token = new Token {Type = _symbols[multipleSymbol], Lexeme = multipleSymbol};
                    return SetTokenLineCol(token);
                }
                CurrentPointer--;
                _currentCol--;
                multipleSymbol = currentChar + GetCurrentSymbol().ToString();
                if (_symbols.ContainsKey(multipleSymbol))
                {
                    CurrentPointer++;
                    _currentCol++;
                    token = new Token {Type = _symbols[multipleSymbol], Lexeme = multipleSymbol};
                    return SetTokenLineCol(token);
                }

                if (multipleSymbol == "%>")
                {
                    _htmlMode = true;
                    CurrentPointer++;
                    _currentCol++;
                    continue;
                }

                token = new Token {Type = _symbols[currentChar.ToString()], Lexeme = currentChar.ToString()};
                return SetTokenLineCol(token);
            }
        }
    }
}