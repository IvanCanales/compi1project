﻿namespace ProyectoCompi1.Lexer
{
    public partial class Lexer
    {
        private void GetComment()
        {
            var nextSymbol = GetCurrentSymbol();
            switch (nextSymbol)
            {
                case '/':
                    CurrentPointer++;
                    _currentCol++;
                    GetLineComment();
                    return;
                case '*':
                    CurrentPointer++;
                    _currentCol++;
                    GetBlockComment();
                    return;
                default:
                    return;
            }
        }

        private void GetBlockComment()
        {
            var lexeme = "/*";
            var tmp1 = GetCurrentSymbol();
            while (!(lexeme.EndsWith("*") && tmp1 == '/'))
            {
                if (tmp1 == '\0')
                    throw new LexerException("Mising close comment statement.");

                lexeme += tmp1;
                CurrentPointer++;
                if (tmp1 == 0x0a)
                {
                    _currentLine++;
                    _currentCol = 1;
                }
                else
                {
                    _currentCol++;
                }
                tmp1 = GetCurrentSymbol();
            }
            CurrentPointer++;
            _currentCol++;
        }

        private void GetLineComment()
        {
            var tmp1 = GetCurrentSymbol();
            while (tmp1 != 0x0a && tmp1 != '\0')
            {
                CurrentPointer++;
                _currentCol++;
                tmp1 = GetCurrentSymbol();
            }
        }
    }
}