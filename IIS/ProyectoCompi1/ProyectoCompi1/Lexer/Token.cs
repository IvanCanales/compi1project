﻿namespace ProyectoCompi1
{
    public class Token
    {
        public TokenTypes Type { get; set; }
        public string Lexeme { get; set; }
        public int Line { get; set; }
        public int Col { get; set; }

        public override string ToString()
        {
            return "Found Lexeme: " + Lexeme + " Type: " + Type + " Line: " + Line + " Col: " + Col;
        }
    }
}