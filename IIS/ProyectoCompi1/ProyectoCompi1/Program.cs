using System;
using System.Collections.Generic;
using System.Linq;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;
using ProyectoCompi1.TypeSpecifiers;

namespace ProyectoCompi1
{
    internal class Program
    {
        private static void Main()
        {
            const string fileName = "C://Users//david//Desktop//Compiladores 1//RevisionAvance5.txt";
            try
            {
                var file = System.IO.File.ReadAllText(fileName);
                var lexer = new Lexer.Lexer(file);
                var parser = new Parser(lexer);
                var stmtList = parser.Parse();
                foreach (var stm in stmtList)
                {
                    var func = stm as FunctionDeclarationStatement;
                    func?.DeclareFunctionSemantic();
                }
                foreach (var stm in stmtList)
                {
                    var returnValue = stm.EvaluateSemantic();
                    if (returnValue != null && ContainsLabel(returnValue))
                        throw new SemanticException("Loop statement not accepted", stm.CurrentToken);
                }
                foreach (var statementNode in stmtList)
                {
                    statementNode.Interpret();
                }
                Console.WriteLine("Success\n".ToUpper());
            }
            catch (Exception e)
            {
                Console.WriteLine( "\n" + e.GetType().Name.ToUpper() + ": " + e.Message.ToUpper() + "\n");
            }
            Console.ReadKey();
        }

        private static bool ContainsLabel(List<TypeNode> types)
        {
            return types.Any(IsLabel);
        }

        private static bool IsLabel(TypeNode type)
        {
            return type is CaseNode || type is DefaultNode 
                || type is BreakNode || type is ContinueNode
                || type is ReturnNode;
        }
    }
}