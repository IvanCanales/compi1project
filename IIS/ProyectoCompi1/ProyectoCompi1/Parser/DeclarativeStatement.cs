﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;

namespace ProyectoCompi1
{
    public partial class Parser
    {
        private StatementNode DeclarativeStatement()
        {
            if (_varDeclarationStmt.Contains(_currentToken.Type))
            {
                return FunOrVarDeclarationStatement();
            }
            else switch (_currentToken.Type)
                {
                    case TokenTypes.Enum:
                        return EnumStatement();
                    case TokenTypes.LibInclude:
                        return IncludeStatement();
                    default:
                        throw new ParserException("Declarative Statement Expected.");
                }
        }

        private StatementNode StructStatement(IdNode id)
        {
            var currentToken = _currentToken;
            var statement = StructCompundStatement();
            EndStatement();
            return new StructStatementNode(id, statement, currentToken);
        }

        private StatementNode StructCompundStatement()
        {
            var currentToken = _currentToken;
            switch (_currentToken.Type)
            {
                case TokenTypes.OpenBrackets:
                    OpenBrackets();
                    var stm = VarDeclarationListOpt();
                    CloseBrackets();
                    return new StructCompoundStatement(stm, currentToken);
                default:
                    return null;
            }
        }

        private List<VarDeclarationStatement> VarDeclarationListOpt()
        {
            if (_varDeclarationStmt.Contains(_currentToken.Type))
            {
                var varDecl = VarDeclarationStatement();
                var varList = VarDeclarationListOpt();
                varList.Insert(0,varDecl);
                return varList;
            }
            else
            {
                return new List<VarDeclarationStatement>();
            }
        }

        private StatementNode EnumStatement()
        {
            if (_currentToken.Type == TokenTypes.Enum)
            {
                var currentToken = _currentToken;
                ConsumeToken();
                var Enum = OptionalId();
                Enum.CurrentToken = currentToken;
                return Enum;
            }
            else
                throw new ParserException("Enum statement expected.");
        }

        private StatementNode OptionalId()
        {
            if (_currentToken.Type == TokenTypes.Id)
            {
                var id = GetId();
                var enumNode = OptionalIdHelper();
                enumNode.Id = id;
                return enumNode;
            }
            else
                return OptionalIdSecondHelper();
        }

        private EnumStatementNode OptionalIdHelper()
        {
            if (_currentToken.Type == TokenTypes.OpenBrackets)
                return OptionalIdSecondHelper();
            else
            {
                var enumAssigner = EnumAssigners();
                EndStatement();
                return new EnumStatementNode(null, enumAssigner);
            }
        }

        private EnumStatementNode OptionalIdSecondHelper()
        {
            OpenBrackets();
            var enumerators = Enumerators();
            CloseBrackets();
            var asssigners = EnumAssigners();
            EndStatement();
            return new EnumStatementNode(enumerators, asssigners);
        }

        private EnumAssigner EnumAssigners()
        {
            if (_currentToken.Type == TokenTypes.Id)
                return EnumAssigner();
            else
            {
                return null;
            }
        }

        private EnumAssigner EnumAssigner()
        {
            var id = GetId();
            var enumAssign = EnumAssignerAssignation();
            enumAssign.id = id;
            return enumAssign;
        }

        private EnumAssigner EnumAssignerAssignation()
        {
            if (_currentToken.Type == TokenTypes.OpAssigEqu)
            {
                ConsumeToken();
                var value = ValueAssigned();
                var chain = Chain();
                return new EnumAssigner(value, chain);
            }
            else
                return new EnumAssigner(null, Chain());
        }

        private ExpressionNode ValueAssigned()
        {
            if (_valueAssigned.Contains(_currentToken.Type))
                return GetValueAssigned();
            else
                throw new ParserException("value assigned expected.");
        }

        private EnumAssigner Chain()
        {
            if (_currentToken.Type == TokenTypes.Comma)
            {
                ConsumeToken();
                return EnumAssigner();
            }
            else
            {
                return null;
            }
        }

        private Enumerator Enumerators()
        {
            if (_currentToken.Type == TokenTypes.Id)
                return Enumerator();
            else
            {
                return null;
            }
        }

        private Enumerator Enumerator()
        {
            var currentToken = _currentToken;
            var id = GetId();
            var enumerator = EnumeratorAssign();
            enumerator.id = id;
            enumerator.CurrentToken = currentToken;
            return enumerator;
        }

        private Enumerator EnumeratorAssign()
        {
            if (_currentToken.Type == TokenTypes.OpAssigEqu)
            {
                ConsumeToken();
                var value = GetLiteral();
                var nextEnum = EnumeratorChain();
                return new Enumerator(value, nextEnum);
            }
            else
                return new Enumerator(null, EnumeratorChain());
        }

        private Enumerator EnumeratorChain()
        {
            if (_currentToken.Type == TokenTypes.Comma)
            {
                ConsumeToken();
                return Enumerator();
            }
            else
            {
                return null;
            }
        }

        private StatementNode FunOrVarDeclarationStatement()
        {
            var currentToken = _currentToken;
            if (_typeSpecifiers.Contains(_currentToken.Type))
            {
                var type = TypeSpecifier();
                if (type is StructNode && _currentToken.Type == TokenTypes.OpenBrackets)
                {
                    var structType = type as StructNode;
                    return StructStatement(structType.Id);
                }
                var isConst = ConstList(false);
                var level = PointerList(0);
                var id = GetId();
                var statement = FunOrVarDeclarationStatementHelper();
                statement.IsConst = isConst;
                statement.Type = type;
                statement.Level = level;
                statement.Id = id;
                statement.CurrentToken = currentToken;
                return statement;
            }
            else if (_currentToken.Type == TokenTypes.Constant)
            {
                var isConst = ConstList(false);
                var type = TypeSpecifier();
                var level = PointerList(0);
                var id = GetId();
                var statement = FunOrVarDeclarationStatementHelper();
                statement.IsConst = isConst;
                statement.Type = type;
                statement.Level = level;
                statement.Id = id;
                statement.CurrentToken = currentToken;
                return statement;
            }
            else
                throw new ParserException("fun or var declaration statement Expected.");
        }

        private FunOrVarStatement FunOrVarDeclarationStatementHelper()
        {
            switch (_currentToken.Type)
            {
                case TokenTypes.OpenParenthesis:
                    ConsumeToken();
                    var param = Params();
                    CloseParenthesis();
                    var statement = CompoundStatement();
                    return new FunctionDeclarationStatement(param, statement);
                default:
                    var arr = IsArray();
                    var variable = Initializer();
                    var declList = DeclarationList();
                    EndStatement();
                    variable.Array = arr;
                    variable.DeclarationList = declList;
                    return variable;
            }
        }

        private Param Params()
        {
            if (_varDeclarationStmt.Contains(_currentToken.Type))
                return ParamList();
            else
            {
                return null;
            }
        }

        private Param ParamList()
        {
            var currentToken = _currentToken;
            var param = Param();
            var nextParam = ParamListHelper();
            return new Param(param, nextParam) {CurrentToken = currentToken};
        }

        private Param Param()
        {
            var currentToken = _currentToken;
            if (_typeSpecifiers.Contains(_currentToken.Type))
            {
                var type = TypeSpecifier();
                var isConst = ConstList(false);
                var level = PointerList(0);
                var id = GetId();
                var arr = IsArray();
                return new Param(type, isConst, level, id, arr, currentToken);
            }
            else if (_currentToken.Type == TokenTypes.Constant)
            {
                var isConst = ConstList(false);
                var type = TypeSpecifier();
                var level = PointerList(0);
                var id = GetId();
                var arr = IsArray();
                return new Param(type, isConst, level, id, arr, currentToken);
            }
            else
                throw new ParserException("Parameter expected.");
        }

        private Param ParamListHelper()
        {
            if (_currentToken.Type == TokenTypes.Comma)
            {
                ConsumeToken();
                return ParamList();
            }
            else
            {
                return null;
            }
        }
    }
}
