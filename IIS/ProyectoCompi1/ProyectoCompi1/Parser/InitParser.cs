﻿using System.Collections.Generic;

namespace ProyectoCompi1
{
    public partial class Parser
    {
        private readonly Lexer.Lexer _lexer;
        private Token _currentToken;

        private readonly List<TokenTypes> _typeSpecifiers = new List<TokenTypes>();
        private readonly List<TokenTypes> _intLiterals = new List<TokenTypes>();
        private readonly List<TokenTypes> _literals = new List<TokenTypes>();
        private readonly List<TokenTypes> _postfixOperators = new List<TokenTypes>();
        private readonly List<TokenTypes> _unaryOperators = new List<TokenTypes>();
        private readonly List<TokenTypes> _shiftOperators = new List<TokenTypes>();
        private readonly List<TokenTypes> _relationalOperators = new List<TokenTypes>();
        private readonly List<TokenTypes> _equalityOperators = new List<TokenTypes>();
        private readonly List<TokenTypes> _assignmentOperators = new List<TokenTypes>();

        private readonly List<TokenTypes> _assignmentExpression = new List<TokenTypes>();
        private readonly List<TokenTypes> _primaryExpression = new List<TokenTypes>();
        private readonly List<TokenTypes> _accessLinks = new List<TokenTypes>();
        private readonly List<TokenTypes> _valueAssigned = new List<TokenTypes>();

        private readonly List<TokenTypes> _basicStatements = new List<TokenTypes>();
        private readonly List<TokenTypes> _expressionStmt = new List<TokenTypes>();
        private readonly List<TokenTypes> _compoundStmt = new List<TokenTypes>();
        private readonly List<TokenTypes> _selectionStmt = new List<TokenTypes>();
        private readonly List<TokenTypes> _iterationStmt = new List<TokenTypes>();
        private readonly List<TokenTypes> _jumpStmt = new List<TokenTypes>();
        private readonly List<TokenTypes> _varDeclarationStmt = new List<TokenTypes>();
        private readonly List<TokenTypes> _labeledStmt = new List<TokenTypes>();

        private readonly List<TokenTypes> _declarativeStatements = new List<TokenTypes>();

        private readonly List<TokenTypes> _statements = new List<TokenTypes>();

        public Parser(Lexer.Lexer lexer)
        {
            _lexer = lexer;
            _currentToken = _lexer.GetNextToken();
            InitLists();
        }

        private void InitLists()
        {
            PopulateIntLiterals();
            PopulatePostfixOperators();
            PopulateShiftOperators();
            PopulateRelationalOperators();
            PopulateEqualityOperators();
            PopulateLiterals();
            PopulateTypeSpecifiers();
            PopulateUnaryOperators();
            PopulateAssignmentOperators();

            PopulateAssignmentExpression();
            PopulatePrimaryExpression();
            PopulateInBuilt();
            PopulateAccessLinks();
            PopulateValueAssigned();

            PopulateBasicStatements();
            PopulateDeclarativeStatements();

            PopulateStatements();
        }

        private void PopulateEqualityOperators()
        {
            _equalityOperators.Add(TokenTypes.OpRelEqu);
            _equalityOperators.Add(TokenTypes.OpRelNequ);
        }

        private void PopulateRelationalOperators()
        {
            _relationalOperators.Add(TokenTypes.OpRelLs);
            _relationalOperators.Add(TokenTypes.OpRelLsEqu);
            _relationalOperators.Add(TokenTypes.OpRelGtr);
            _relationalOperators.Add(TokenTypes.OpRelGtrEqu);
        }

        private void PopulateShiftOperators()
        {
            _shiftOperators.Add(TokenTypes.OpBitL);
            _shiftOperators.Add(TokenTypes.OpBitR);
        }

        private void PopulatePostfixOperators()
        {
            _postfixOperators.Add(TokenTypes.OpenParenthesis);
            _postfixOperators.Add(TokenTypes.OpInc);
            _postfixOperators.Add(TokenTypes.OpDec);
        }

        private void PopulateIntLiterals()
        {
            _intLiterals.Add(TokenTypes.LitHex);
            _intLiterals.Add(TokenTypes.LitOctal);
            _intLiterals.Add(TokenTypes.LitDecimal);
        }

        private void PopulateValueAssigned()
        {
            _valueAssigned.AddRange(_literals);
            _valueAssigned.Add(TokenTypes.Id);
        }

        private void PopulateAssignmentExpression()
        {
            _assignmentExpression.Add(TokenTypes.OpInterrogation);
            _assignmentExpression.Add(TokenTypes.OpLogAnd);
            _assignmentExpression.Add(TokenTypes.OpLogOr);
            _assignmentExpression.Add(TokenTypes.OpBitOr);
            _assignmentExpression.Add(TokenTypes.OpBitXor);
            _assignmentExpression.Add(TokenTypes.Id);
            _assignmentExpression.Add(TokenTypes.OpenParenthesis);
            _assignmentExpression.AddRange(_equalityOperators);
            _assignmentExpression.AddRange(_relationalOperators);
            _assignmentExpression.AddRange(_shiftOperators);
            _assignmentExpression.AddRange(_unaryOperators);
            _assignmentExpression.AddRange(_literals);
        }

        private void PopulateAccessLinks()
        {
            _accessLinks.Add(TokenTypes.OpenArr);
            _accessLinks.Add(TokenTypes.DeRefArrow);
            _accessLinks.Add(TokenTypes.DeRefDot);
        }

        private void PopulatePrimaryExpression()
        {
            _primaryExpression.Add(TokenTypes.Id);
            _primaryExpression.AddRange(_literals);
            _primaryExpression.Add(TokenTypes.OpenParenthesis);
        }

        private void PopulateTypeSpecifiers()
        {
            _typeSpecifiers.Add(TokenTypes.DataBoolean);
            _typeSpecifiers.Add(TokenTypes.DataCharacter);
            _typeSpecifiers.Add(TokenTypes.DataDate);
            _typeSpecifiers.Add(TokenTypes.DataFloat);
            _typeSpecifiers.Add(TokenTypes.DataInteger);
            _typeSpecifiers.Add(TokenTypes.DataString);
            _typeSpecifiers.Add(TokenTypes.DataVoid);
            _typeSpecifiers.Add(TokenTypes.Struct);
        }

        private void PopulateLiterals()
        {
            _literals.Add(TokenTypes.LitHex);
            _literals.Add(TokenTypes.LitOctal);
            _literals.Add(TokenTypes.LitDecimal);
            _literals.Add(TokenTypes.LitChar);
            _literals.Add(TokenTypes.LitDate);
            _literals.Add(TokenTypes.LitFalse);
            _literals.Add(TokenTypes.LitFloat);
            _literals.Add(TokenTypes.LitString);
            _literals.Add(TokenTypes.LitTrue);
        }

        private void PopulateUnaryOperators()
        {
            _unaryOperators.Add(TokenTypes.OpBitAnd);
            _unaryOperators.Add(TokenTypes.OpMul);
            _unaryOperators.Add(TokenTypes.OpSum);
            _unaryOperators.Add(TokenTypes.OpSub);
            _unaryOperators.Add(TokenTypes.OpBitComp);
            _unaryOperators.Add(TokenTypes.OpLogNot);
        }

        private void PopulateAssignmentOperators()
        {
            _assignmentOperators.Add(TokenTypes.OpAssigEqu);
            _assignmentOperators.Add(TokenTypes.OpAssigSum);
            _assignmentOperators.Add(TokenTypes.OpAssigSub);
            _assignmentOperators.Add(TokenTypes.OpAssigMul);
            _assignmentOperators.Add(TokenTypes.OpAssigDiv);
            _assignmentOperators.Add(TokenTypes.OpAssigMod);
            _assignmentOperators.Add(TokenTypes.OpAssigBitL);
            _assignmentOperators.Add(TokenTypes.OpAssigBitR);
            _assignmentOperators.Add(TokenTypes.OpAssigBitAnd);
            _assignmentOperators.Add(TokenTypes.OpAssigBitXor);
            _assignmentOperators.Add(TokenTypes.OpAssigBitOr);
        }

        private void PopulateStatements()
        {
            _statements.AddRange(_basicStatements);
            _statements.AddRange(_declarativeStatements);
        }

        private void PopulateDeclarativeStatements()
        {
            _declarativeStatements.AddRange(_varDeclarationStmt);
            _declarativeStatements.Add(TokenTypes.Struct);
            _declarativeStatements.Add(TokenTypes.Enum);
            _declarativeStatements.Add(TokenTypes.LibInclude);
        }

        private void PopulateBasicStatements()
        {
            PopulateExpressionStmt();
            PopulateCompoundStmt();
            PopulateSelectionStmt();
            PopulateIterationStmt();
            PopulateJumpStmt();
            PopulateVarDeclarationStmt();
            PopulateLabeledStmt();

            _basicStatements.AddRange(_expressionStmt);
            _basicStatements.AddRange(_compoundStmt);
            _basicStatements.AddRange(_selectionStmt);
            _basicStatements.AddRange(_iterationStmt);
            _basicStatements.AddRange(_jumpStmt);
            _basicStatements.AddRange(_varDeclarationStmt);
            _basicStatements.AddRange(_labeledStmt);
            _basicStatements.Add(TokenTypes.Print);
            _basicStatements.Add(TokenTypes.Html);
        }

        private void PopulateLabeledStmt()
        {
            _labeledStmt.Add(TokenTypes.SwitchCase);
            _labeledStmt.Add(TokenTypes.SwitchDefault);
        }

        private void PopulateVarDeclarationStmt()
        {
            _varDeclarationStmt.AddRange(_typeSpecifiers);
            _varDeclarationStmt.Add(TokenTypes.Constant);
        }

        private void PopulateJumpStmt()
        {
            _jumpStmt.Add(TokenTypes.Return);
            _jumpStmt.Add(TokenTypes.Continue);
            _jumpStmt.Add(TokenTypes.Break);
        }

        private void PopulateIterationStmt()
        {
            _iterationStmt.Add(TokenTypes.LoopDoWhile);
            _iterationStmt.Add(TokenTypes.LoopFor);
            _iterationStmt.Add(TokenTypes.LoopWhile);
        }

        private void PopulateSelectionStmt()
        {
            _selectionStmt.Add(TokenTypes.If);
            _selectionStmt.Add(TokenTypes.Switch);
        }

        private void PopulateCompoundStmt()
        {
            _compoundStmt.Add(TokenTypes.OpenBrackets);
        }

        private void PopulateExpressionStmt()
        {
            _expressionStmt.Add(TokenTypes.EndStm);
            _expressionStmt.AddRange(_unaryOperators);
            _expressionStmt.AddRange(_postfixOperators);
            _expressionStmt.AddRange(_literals);
            _expressionStmt.Add(TokenTypes.Id);
        }

        private void PopulateInBuilt()
        {
            _primaryExpression.Add(TokenTypes.GetForm);
            _primaryExpression.Add(TokenTypes.GetQuery);
            _primaryExpression.Add(TokenTypes.GetMethod);
            _primaryExpression.Add(TokenTypes.Atoi);
        }
    }
}
