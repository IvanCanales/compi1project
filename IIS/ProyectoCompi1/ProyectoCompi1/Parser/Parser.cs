﻿using System;
using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Tree.Literal;

namespace ProyectoCompi1
{
    public partial class Parser
    {
        public List<StatementNode> Parse()
        {
            try
            {
                var program = Program();
                if (_currentToken.Type != TokenTypes.Eof)
                    throw new ParserException("Eof expected");
                return program;
            }
            catch (LexerException e)
            {
                throw new LexerException((e.Message + " found " + _currentToken).ToUpper());
            }
            catch(ParserException e)
            {
                throw new ParserException((e.Message + " found " + _currentToken).ToUpper());
            }
        }

        private List<StatementNode> Program()
        {
            var statementList = StatementList();
            return statementList;
        }

        private List<StatementNode> StatementList()
        {
            if (_statements.Contains(_currentToken.Type))
            {
                var statement = Statement();
                var s1 = StatementList();
                s1.Insert(0, statement);
                return s1;
            }
            else
            {
                return new List<StatementNode>();
            }
        }

        private StatementNode Statement()
        {
            if (_declarativeStatements.Contains(_currentToken.Type))
            {
                return DeclarativeStatement();
            }
            else if (_basicStatements.Contains(_currentToken.Type))
                return BasicStatement();
            else
                throw new ParserException("Statement expected.");
        }

        private string ConsumeToken()
        {
            var lexeme = _currentToken.Lexeme;
            _currentToken = _lexer.GetNextToken();
            return lexeme;
        }

        private void EndStatement()
        {
            if (_currentToken.Type != TokenTypes.EndStm)
                throw new ParserException("End statement ; expected.");
            ConsumeToken();
        }

        private void OpenBrackets()
        {
            if (_currentToken.Type != TokenTypes.OpenBrackets)
                throw new ParserException("Open { expected.");
            ConsumeToken();
        }

        private IdNode GetId()
        {
            if (_currentToken.Type != TokenTypes.Id)
                throw new ParserException("Id expected.");
            var id = _currentToken.Lexeme;
            var current = _currentToken;
            ConsumeToken();
            return new IdNode(id, current);
        }

        private AssignmentOperatorNode GetAssignOperator()
        {
            var currentToken = _currentToken;
            switch (_currentToken.Type)
            {
                case TokenTypes.OpAssigEqu:
                    return new AssignmentEqualityNode(currentToken);
                case TokenTypes.OpAssigSum:
                    return new AssignmentSumNode(currentToken);
                case TokenTypes.OpAssigSub:
                    return new AssignmentSubNode(currentToken);
                case TokenTypes.OpAssigMul:
                    return new AssignmentMulNode(currentToken);
                case TokenTypes.OpAssigDiv:
                    return new AssignmentDivNode(currentToken);
                case TokenTypes.OpAssigMod:
                    return new AssignmentModNode(currentToken);
                case TokenTypes.OpAssigBitL:
                    return new AssignmentBitLeftNode(currentToken);
                case TokenTypes.OpAssigBitR:
                    return new AssignmentBitRightNode(currentToken);
                case TokenTypes.OpAssigBitAnd:
                    return new AssignmentBitAndNode(currentToken);
                case TokenTypes.OpAssigBitXor:
                    return new AssignmentBitXorNode(currentToken);
                case TokenTypes.OpAssigBitOr:
                    return new AssignmentBitOrNode(currentToken);
                default:
                    return null;
            }
        }

        private LiteralNode GetLiteral()
        {
            if (!_literals.Contains(_currentToken.Type))
                throw new ParserException("literal expected.");
            string literal;
            var currentToken = _currentToken;
            switch (_currentToken.Type)
            {
                case TokenTypes.LitHex:
                case TokenTypes.LitOctal:
                case TokenTypes.LitDecimal:
                    literal = ConsumeToken();
                    return new LiteralIntNode(literal, currentToken);
                case TokenTypes.LitChar:
                    literal = ConsumeToken();
                    return new LiteralCharNode(literal, currentToken);
                case TokenTypes.LitDate:
                    literal = ConsumeToken();
                    return new LiteralDateNode(literal, currentToken);
                case TokenTypes.LitFalse:
                case TokenTypes.LitTrue:
                    literal = ConsumeToken();
                    return new LiteralBoolNode(literal, currentToken);
                case TokenTypes.LitFloat:
                    literal = ConsumeToken();
                    return new LiteralFloatNode(literal, currentToken);
                case TokenTypes.LitString:
                    literal = ConsumeToken();
                    return new LiteralStringNode(literal, currentToken);
                default:
                    return null;
            }
        }

        private TypeNode TypeSpecifier()
        {
            if (_typeSpecifiers.Contains(_currentToken.Type))
            {
                if (_currentToken.Type == TokenTypes.Struct)
                {
                    ConsumeToken();
                    var id = GetId();
                    return new StructNode(id);
                }
                else
                {
                    switch (_currentToken.Type)
                    {
                        case TokenTypes.DataBoolean:
                            ConsumeToken();
                            return new BooleanNode();
                        case TokenTypes.DataCharacter:
                            ConsumeToken();
                            return new CharacterNode();
                        case TokenTypes.DataDate:
                            ConsumeToken();
                            return new DateNode();
                        case TokenTypes.DataFloat:
                            ConsumeToken();
                            return new FloatNode();
                        case TokenTypes.DataInteger:
                            ConsumeToken();
                            return new IntegerNode();
                        case TokenTypes.DataString:
                            ConsumeToken();
                            return new StringNode();
                        case TokenTypes.DataVoid:
                            ConsumeToken();
                            return new VoidNode();
                        default:
                            return null;
                    }
                }
            }
            else
                throw new ParserException("type specifier expected.");
        }

        private void CloseBrackets()
        {
            if (_currentToken.Type != TokenTypes.CloseBrackets)
                throw new ParserException("close } expected.");
            ConsumeToken();
        }

        private void CloseParenthesis()
        {
            if (_currentToken.Type != TokenTypes.CloseParenthesis)
                throw new ParserException("Close ) expected.");
            ConsumeToken();
        }

        private void OpenParenthesis()
        {
            if (_currentToken.Type != TokenTypes.OpenParenthesis)
                throw new ParserException("Open ( expected.");
            ConsumeToken();
        }

        private void CloseArray()
        {
            if (_currentToken.Type != TokenTypes.CloseArr)
                throw new ParserException("Close array expected");
            ConsumeToken();
        }

        private void GetColon()
        {
            if (_currentToken.Type != TokenTypes.Colon)
                throw new ParserException("symbol : expected.");
            ConsumeToken();
        }

        private void GetWhile()
        {
            if (_currentToken.Type != TokenTypes.LoopWhile)
                throw new ParserException("while statement expected.");
            ConsumeToken();
        }

        private ExpressionNode GetValueAssigned()
        {
            if (_currentToken.Type == TokenTypes.Id)
            {
                var current = _currentToken;
                var value = ConsumeToken();
                return new IdNode(value, current);
            }
            else
                return GetLiteral();
        }
    }
}