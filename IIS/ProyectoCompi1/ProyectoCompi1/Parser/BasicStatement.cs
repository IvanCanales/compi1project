﻿using System.Collections.Generic;
using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Arbol.Statements.IterationStatement;
using ProyectoCompi1.Tree.Statements.BasicStatement;
using ProyectoCompi1.Tree.Statements.FunOrVarStatement;

namespace ProyectoCompi1
{
    public partial class Parser
    {
        private StatementNode BasicStatement()
        {
            if (_expressionStmt.Contains(_currentToken.Type))
            {
                return ExpressionStatement();
            }
            else if (_compoundStmt.Contains(_currentToken.Type))
            {
                return CompoundStatement();
            }
            else if (_selectionStmt.Contains(_currentToken.Type))
                return SelectionStatement();
            else if (_iterationStmt.Contains(_currentToken.Type))
            {
                return IterationStatement();
            }
            else if (_jumpStmt.Contains(_currentToken.Type))
            {
                return JumpStatement();
            }
            else if (_varDeclarationStmt.Contains(_currentToken.Type))
            {
                return VarDeclarationStatement();
            }
            else if (_labeledStmt.Contains(_currentToken.Type))
            {
                return LabeledStatement();
            }
            else if (_currentToken.Type == TokenTypes.Print)
            {
                return PrintStatement();
            }
            else if (_currentToken.Type == TokenTypes.Html)
            {
                return HtmlStatement();
            }
            else
                throw new ParserException("basic statement expected.");
        }

        private StatementNode PrintStatement()
        {
            var currentToken = _currentToken;
            if(_currentToken.Type != TokenTypes.Print)
                throw new ParserException("Expected Print token");
            ConsumeToken();
            OpenParenthesis();
            var exp = Expression();
            CloseParenthesis();
            EndStatement();
            return new PrintStatement(exp, currentToken);
        }

        private StatementNode HtmlStatement()
        {
            var currentToken = _currentToken;
            ConsumeToken();
            return new HtmlStatement(currentToken.Lexeme, currentToken);
        }

        private StatementNode LabeledStatement()
        {
            List<StatementNode> basicStmList;
            Token currentToken;
            switch (_currentToken.Type)
            {
                case TokenTypes.SwitchCase:
                    currentToken = _currentToken;
                    ConsumeToken();
                    var exp = LogicalOrExpression();
                    GetColon();
                    basicStmList = BasicStatementList();
                    return new CaseStatementNode(exp, basicStmList, currentToken);
                case TokenTypes.SwitchDefault:
                    currentToken = _currentToken;
                    ConsumeToken();
                    GetColon();
                    basicStmList = BasicStatementList();
                    return new DefaultStatementNode(basicStmList, currentToken);
                default:
                    throw new ParserException("label statement expected.");
            }
        }

        private List<StatementNode> BasicStatementList()
        {
            if (_basicStatements.Contains(_currentToken.Type))
            {
                var basicStatement = BasicStatement();
                var basicStmList = BasicStatementList();
                basicStmList.Insert(0,basicStatement);
                return basicStmList;
            }
            else
            {
                return new List<StatementNode>();
            }
        }

        private StatementNode IncludeStatement()
        {
            if (_currentToken.Type != TokenTypes.LibInclude)
                throw new ParserException("include expected.");
            ConsumeToken();
            if (_currentToken.Type != TokenTypes.LitString)
                throw new ParserException("string literal expected after include.");
            var currentToken = _currentToken;
            var literal = ConsumeToken();
            return new IncludeStatement(literal, currentToken);
        }

        private VarDeclarationStatement VarDeclarationStatement()
        {
            Token currentToken = _currentToken;
            if (_typeSpecifiers.Contains(_currentToken.Type))
            {
                var type = TypeSpecifier();
                var isConst = ConstList(false);
                var level = PointerList(0);
                var id = GetId();
                var arr = IsArray();
                var varDecl = Initializer();
                var declList = DeclarationList();
                EndStatement();
                varDecl.Type = type;
                varDecl.IsConst = isConst;
                varDecl.Level = level;
                varDecl.Id = id;
                varDecl.Array = arr;
                varDecl.DeclarationList = declList;
                varDecl.CurrentToken = currentToken;
                return varDecl;
            }
            else if (_currentToken.Type == TokenTypes.Constant)
            {
                var isConst = ConstList(false);
                var type = TypeSpecifier();
                var level = PointerList(0);
                var id = GetId();
                var arr = IsArray();
                var varDecl = Initializer();
                var declList = DeclarationList();
                EndStatement();
                varDecl.Type = type;
                varDecl.IsConst = isConst;
                varDecl.Level = level;
                varDecl.Id = id;
                varDecl.Array = arr;
                varDecl.DeclarationList = declList;
                varDecl.CurrentToken = currentToken;
                return varDecl;
            }
            else
                throw new ParserException("wrong var declaration format.");
        }

        private VarDeclarationStatement Initializer()
        {
            if (_currentToken.Type == TokenTypes.OpAssigEqu)
            {
                ExpressionNode value;
                ConsumeToken();
                if (_currentToken.Type == TokenTypes.OpenBrackets)
                {
                    ConsumeToken();
                    value = ArrayInitializer();
                    CloseBrackets();
                }
                else
                    value = AssignmentExpression();
                return new VarDeclarationStatement(value);
            }
            else
            {
                return new VarDeclarationStatement();
            }
        }

        private ExpressionNode ArrayInitializer()
        {
            if (_currentToken.Type == TokenTypes.OpenBrackets)
                return ArrayInitializerHelper();
            else
                return Expression();
        }

        private ExpressionNode ArrayInitializerHelper()
        {
            var currentToken = _currentToken;
            OpenBrackets();
            var value1 = ArrayInitializer();
            CloseBrackets();
            var value2 = ArrayInitializerSecondHelper();
            return new ArrayInitializer(value1, value2, currentToken);
        }

        private ExpressionNode ArrayInitializerSecondHelper()
        {
            if (_currentToken.Type == TokenTypes.Comma)
            {
                ConsumeToken();
                return ArrayInitializerHelper();
            }
            else
            {
                return null;
            }
        }

        private List<VarDeclarationStatement> DeclarationList()
        {
            if (_currentToken.Type == TokenTypes.Comma)
            {
                ConsumeToken();
                var decl = Declaration();
                var declList = DeclarationList();
                declList.Insert(0, decl);
                return declList;
            }
            else
            {
                return new List<VarDeclarationStatement>();
            }
        }

        private VarDeclarationStatement Declaration()
        {
            var id = GetId();
            var arr = IsArray();
            var variable = Initializer();
            variable.Id = id;
            variable.Array = arr;
            return variable;
        }

        private ArrayNode IsArray()
        {
            if (_currentToken.Type == TokenTypes.OpenArr)
            {
                ConsumeToken();
                var value = IntOpcional();
                CloseArray();
                var nextArr = IsArrayHelper();
                return new ArrayNode(value, nextArr);
            }
            else
            {
                return new ArrayNode();
            }
        }

        private ArrayNode IsArrayHelper()
        {
            if (_currentToken.Type == TokenTypes.OpenArr)
            {
                ConsumeToken();
                var currentToken = _currentToken;
                ExpressionNode value;
                if (_intLiterals.Contains(_currentToken.Type))
                {
                    value = new LiteralIntNode(ConsumeToken(), currentToken);
                }
                else if (_currentToken.Type == TokenTypes.Id)
                {
                    value = new IdNode(ConsumeToken(), currentToken);
                }
                else
                    throw new ParserException("int literal expected.");
                CloseArray();
                var nextArr = IsArrayHelper();
                return new ArrayNode(value, nextArr);
            }
            else
            {
                return null;
            }
        }

        private ExpressionNode IntOpcional()
        {
            if (_intLiterals.Contains(_currentToken.Type))
            {
                var currentToken = _currentToken;
                var value = ConsumeToken();
                return new LiteralIntNode(value, currentToken);
            }
            else if (_currentToken.Type == TokenTypes.Id)
            {
                var current = _currentToken;
                var value = ConsumeToken();
                return new IdNode(value, current);
            }
            else
            {
                return null;
            }
        }

        private int PointerList(int level)
        {
            if (_currentToken.Type == TokenTypes.OpMul)
            {
                ConsumeToken();
                return PointerList(level+1);
            }
            else
            {
                return level;
            }
        }

        private bool ConstList(bool isConst)
        {
            if (_currentToken.Type == TokenTypes.Constant)
            {
                ConsumeToken();
                return ConstList(true);
            }
            else
            {
                return isConst;
            }
        }

        private StatementNode JumpStatement()
        {
            var currentToken = _currentToken;
            switch (_currentToken.Type)
            {
                case TokenTypes.Break:
                    ConsumeToken();
                    EndStatement();
                    return new BreakStatementNode(currentToken);
                case TokenTypes.Return:
                    ConsumeToken();
                    var expStm = ExpressionStatement();
                    return new ReturnStatementNode(expStm, currentToken);
                case TokenTypes.Continue:
                    ConsumeToken();
                    EndStatement();
                    return new ContinueStatementNode(currentToken);
                default:
                    throw new ParserException("jump statement expected.");
            }
        }

        private StatementNode IterationStatement()
        {
            switch (_currentToken.Type)
            {
                case TokenTypes.LoopWhile:
                    return WhileStmt();
                case TokenTypes.LoopDoWhile:
                    return DoWhileStmt();
                case TokenTypes.LoopFor:
                    return ForStmt();
                default:
                    throw new ParserException("Iterarion statement expected.");
            }
        }

        private StatementNode ForStmt()
        {
            if (_currentToken.Type != TokenTypes.LoopFor)
                throw new ParserException("for statement expected.");
            ConsumeToken();
            OpenParenthesis();
            var forStm = ForExpressionStmt();
            CloseParenthesis();
            var basicStatement = BasicStatement();
            forStm.BasicStatement = basicStatement;
            return forStm;
        }

        private IterationStatementNode ForExpressionStmt()
        {
            var currentToken = _currentToken;
            if (_typeSpecifiers.Contains(_currentToken.Type))
            {
                var type = TypeSpecifier();
                var level = PointerList(0);
                var id = GetId();
                GetColon();
                var listId = GetId();
                return new ForEachStatementNode(type, level, id, listId, currentToken);
            }
            else if (_expressionStmt.Contains(_currentToken.Type))
            {
                var exp1 = Expression();
                EndStatement();
                var exp2 = Expression();
                EndStatement();
                var exp3 = Expression();
                return new ForStatementNode(exp1, exp2, exp3, currentToken);
            }
            else
                throw new ParserException("wrong for expresssion statement format.");
        }

        private StatementNode DoWhileStmt()
        {
            var currentToken = _currentToken;
            if (_currentToken.Type != TokenTypes.LoopDoWhile)
                throw new ParserException("do while statement expected.");
            ConsumeToken();
            var basicStatement = BasicStatement();
            GetWhile();
            OpenParenthesis();
            var exp = Expression();
            CloseParenthesis();
            EndStatement();
            return new DoWhileStatementNode(exp, basicStatement, currentToken);
        }

        private StatementNode WhileStmt()
        {
            var currentToken = _currentToken;
            GetWhile();
            OpenParenthesis();
            var exp = Expression();
            CloseParenthesis();
            var basicStatement = BasicStatement();
            return new WhileStatementNode(exp, basicStatement, currentToken);
        }

        private StatementNode CompoundStatement()
        {
            var currentToken = _currentToken;
            OpenBrackets();
            var basicStmList = BasicStatementList();
            CloseBrackets();
            return new CompoundStatementNode(basicStmList, currentToken);
        }

        private StatementNode SelectionStatement()
        {
            switch (_currentToken.Type)
            {
                case TokenTypes.If:
                    return IfStmt();
                case TokenTypes.Switch:
                    return SwitchStmt();
                default:
                    throw new ParserException("Selection statement expected.");
            }
        }

        private StatementNode SwitchStmt()
        {
            var currentToken = _currentToken;
            if (_currentToken.Type != TokenTypes.Switch)
                throw new ParserException("switch statement expected.");
            ConsumeToken();
            OpenParenthesis();
            var exp = Expression();
            CloseParenthesis();
            var basicStatement = BasicStatement();
            return new SwitchStatementNode(exp, basicStatement, currentToken);
        }

        private StatementNode IfStmt()
        {
            var currentToken = _currentToken;
            if (_currentToken.Type != TokenTypes.If)
                throw new ParserException("if statement expected.");
            ConsumeToken();
            OpenParenthesis();
            var expression = Expression();
            CloseParenthesis();
            var ifStm = IfStmtHelper(expression);
            ifStm.CurrentToken = currentToken;
            return ifStm;
        }

        private StatementNode IfStmtHelper(ExpressionNode expression)
        {
            var basicStatement = BasicStatement();
            return IfStmtSecondHelper(expression, basicStatement);
        }

        private StatementNode IfStmtSecondHelper(ExpressionNode expression, StatementNode statement)
        {
            if (_currentToken.Type == TokenTypes.Else)
            {
                ConsumeToken();
                var elseStatement = BasicStatement();
                return new IfStatementNode(expression, statement, elseStatement);
            }
            else
            {
                return new IfStatementNode(expression, statement, null);
            }
        }

        private StatementNode ExpressionStatement()
        {
            var currentToken = _currentToken;
            if (_currentToken.Type == TokenTypes.EndStm)
            {
                ConsumeToken();
                return new ExpressionStatementNode(currentToken);
            }
            else if (_expressionStmt.Contains(_currentToken.Type))
            {
                var exp = Expression();
                EndStatement();
                return new ExpressionStatementNode(exp, currentToken);
            }
            else
                throw new ParserException("Condition statement expected.");
        }
    }
}
