﻿using ProyectoCompi1.Arbol.Expression;
using ProyectoCompi1.Arbol.Expression.BinaryOperator;

namespace ProyectoCompi1
{
    public partial class Parser
    {
        private ExpressionNode AssignmentExpression()
        {
            var currentToken = _currentToken;
            var expression = LogicalOrExpression();
            var ternary = TernaryOperator(expression);
            ternary.CurrentToken = currentToken;
            return AssignmentExpressionHelper(ternary);
        }

        private ExpressionNode AssignmentExpressionHelper(ExpressionNode ternaryOperator)
        {
            if (_assignmentOperators.Contains(_currentToken.Type))
            {
                var assignOp = GetAssignOperator();
                ConsumeToken();
                var assig = AssignmentExpression();
                assignOp.LeftOperand = ternaryOperator;
                assignOp.RightOperand = assig;
                return assignOp;
            }
            else
            {
                return ternaryOperator;
            }
        }

        private ExpressionNode TernaryOperator(ExpressionNode conditionalExpression)
        {
            if (_currentToken.Type == TokenTypes.OpInterrogation)
            {
                ConsumeToken();
                var trueExpression = Expression();
                GetColon();
                var falseExpression = AssignmentExpression();
                return new TernaryOperatorNode(conditionalExpression, trueExpression, falseExpression);
            }
            else
            {
                return conditionalExpression;
            }
        }

        private ExpressionNode Expression()
        {
            var currentToken = _currentToken;
            var assign = AssignmentExpression();
            var inlineExp =  InlineExpression(assign);
            inlineExp.CurrentToken = currentToken;
            return inlineExp;
        }

        private ExpressionNode InlineExpression(ExpressionNode assignExpression)
        {
            if (_currentToken.Type == TokenTypes.Comma)
            {
                ConsumeToken();
                var currentToken = _currentToken;
                var exp = AssignmentExpression();
                var inlineExp = InlineExpressionHelper();
                var inlineNode = new InlineExpressionNode(exp, inlineExp) {CurrentToken = currentToken};
                return new InlineExpressionNode(assignExpression, inlineNode);
            }
            else
            {
                return assignExpression;
            }
        }

        private ExpressionNode InlineExpressionHelper()
        {
            if (_currentToken.Type == TokenTypes.Comma)
            {
                ConsumeToken();
                var currentToken = _currentToken;
                var exp = AssignmentExpression();
                var inlineExp = InlineExpressionHelper();
                return new InlineExpressionNode(exp, inlineExp) {CurrentToken = currentToken};
            }
            else
            {
                return null;
            }
        }

        private ExpressionNode LogicalOrExpression()
        {
            var logicalAnd = LogicalAndExpression();
            return LogicalOrExpressionHelper(logicalAnd);
        }

        private ExpressionNode LogicalOrExpressionHelper(ExpressionNode logicalAnd)
        {
            if (_currentToken.Type == TokenTypes.OpLogOr)
            {
                var currentToken = _currentToken;
                ConsumeToken();
                var logicalAnd2 = LogicalAndExpression();
                return new LogicalOrNode(logicalAnd, logicalAnd2) {CurrentToken = currentToken};
            }
            else
            {
                return logicalAnd;
            }
        }

        private ExpressionNode LogicalAndExpression()
        {
            var inclusiveOr = InclusiveOrExpression();
            return LogicalAndExpressionHelper(inclusiveOr);
        }

        private ExpressionNode LogicalAndExpressionHelper(ExpressionNode inclusiveOr)
        {
            if (_currentToken.Type == TokenTypes.OpLogAnd)
            {
                var currentToken = _currentToken;
                ConsumeToken();
                var inclusiveOr2 = InclusiveOrExpression();
                return new LogicalAndNode(inclusiveOr, inclusiveOr2) {CurrentToken = currentToken};
            }
            else
            {
                return inclusiveOr;
            }
        }

        private ExpressionNode InclusiveOrExpression()
        {
            var exclusiveOr = ExclusiveOrExpression();
            return InclusiveOrExpressionHelper(exclusiveOr);
        }

        private ExpressionNode InclusiveOrExpressionHelper(ExpressionNode exclusiveOr)
        {
            if (_currentToken.Type == TokenTypes.OpBitOr)
            {
                var currentToken = _currentToken;
                ConsumeToken();
                var exclusiveOr2 = ExclusiveOrExpression();
                return new InclusiveOrNode(exclusiveOr, exclusiveOr2) {CurrentToken = currentToken};
            }
            else
            {
                return exclusiveOr;
            }
        }

        private ExpressionNode ExclusiveOrExpression()
        {
            var and = AndExpression();
            return ExclusiveOrExpressionHelper(and);
        }

        private ExpressionNode ExclusiveOrExpressionHelper(ExpressionNode and)
        {
            if (_currentToken.Type == TokenTypes.OpBitXor)
            {
                var currentToken = _currentToken;
                ConsumeToken();
                var and2 = AndExpression();
                return new ExclusiveOrNode(and, and2) {CurrentToken = currentToken};
            }
            else
            {
                return and;
            }
        }

        private ExpressionNode AndExpression()
        {
            var equ = EqualityExpression();
            return AndExpressionHelper(equ);
        }

        private ExpressionNode AndExpressionHelper(ExpressionNode equ)
        {
            if (_currentToken.Type == TokenTypes.OpBitAnd)
            {
                var currentToken = _currentToken;
                ConsumeToken();
                var equ2 = EqualityExpression();
                return new BitAndNode(equ, equ2) {CurrentToken = currentToken};
            }
            else
            {
                return equ;
            }
        }

        private ExpressionNode EqualityExpression()
        {
            var rel = RelationalExpression();
            return EqualityExpressionHelper(rel);
        }

        private ExpressionNode EqualityExpressionHelper(ExpressionNode rel)
        {
            var currentToken = _currentToken;
            ExpressionNode rel2;
            switch (_currentToken.Type)
            {
                case TokenTypes.OpRelEqu:
                    ConsumeToken();
                    rel2 = RelationalExpression();
                    return new EqualityEqualNode(rel, rel2) {CurrentToken = currentToken};
                case TokenTypes.OpRelNequ:
                    ConsumeToken();
                    rel2 = RelationalExpression();
                    return new EqualityNotEqualNode(rel, rel2) {CurrentToken = currentToken};
                default:
                    return rel;
            }
        }

        private ExpressionNode RelationalExpression()
        {
            var shift = ShiftExpression();
            return RelationalExpressionHelper(shift);
        }

        private ExpressionNode RelationalExpressionHelper(ExpressionNode shiftExpression)
        {
            var currentToken = _currentToken;
            ExpressionNode shiftExpression2;
            switch (_currentToken.Type)
            {
                case TokenTypes.OpRelLs:
                    ConsumeToken();
                    shiftExpression2 = ShiftExpression();
                    return new RelationalLessNode(shiftExpression, shiftExpression2) {CurrentToken = currentToken};
                case TokenTypes.OpRelGtr:
                    ConsumeToken();
                    shiftExpression2 = ShiftExpression();
                    return new RelationalGreaterNode(shiftExpression, shiftExpression2) {CurrentToken = currentToken};
                case TokenTypes.OpRelLsEqu:
                    ConsumeToken();
                    shiftExpression2 = ShiftExpression();
                    return new RelationalLessEqualNode(shiftExpression, shiftExpression2) {CurrentToken = currentToken};
                case TokenTypes.OpRelGtrEqu:
                    ConsumeToken();
                    shiftExpression2 = ShiftExpression();
                    return new RelationalGreaterEqualNode(shiftExpression, shiftExpression2) {CurrentToken = currentToken};
                default:
                    return shiftExpression;
            }
        }

        private ExpressionNode ShiftExpression()
        {
            var additiveExpression = AdditiveExpression();
            return ShiftExpressionHelper(additiveExpression);
        }

        private ExpressionNode ShiftExpressionHelper(ExpressionNode additiveExpression)
        {
            var currentToken = _currentToken;
            ExpressionNode additiveExpression2;
            switch (_currentToken.Type)
            {
                case TokenTypes.OpBitL:
                    ConsumeToken();
                    additiveExpression2 = AdditiveExpression();
                    return new ShiftLeftNode(additiveExpression, additiveExpression2) {CurrentToken = currentToken};
                case TokenTypes.OpBitR:
                    ConsumeToken();
                    additiveExpression2 = AdditiveExpression();
                    return new ShiftRightNode(additiveExpression, additiveExpression2) {CurrentToken = currentToken};
                default:
                    return additiveExpression;
            }
        }

        private ExpressionNode AdditiveExpression()
        {
            var multExp = MultiplicativeExpression();
            return AdditiveExpressionHelper(multExp);
        }

        private ExpressionNode AdditiveExpressionHelper(ExpressionNode multExp)
        {
            var currentToken = _currentToken;
            ExpressionNode multExp2;
            ExpressionNode addExp;
            switch (_currentToken.Type)
            {
                case TokenTypes.OpSum:
                    ConsumeToken();
                    multExp2 = MultiplicativeExpression();
                    addExp = AdditiveExpressionHelper(multExp2);
                    return new SumNode(multExp, addExp) {CurrentToken = currentToken};
                case TokenTypes.OpSub:
                    ConsumeToken();
                    multExp2 = MultiplicativeExpression();
                    addExp = AdditiveExpressionHelper(multExp2);
                    return new SubNode(multExp, addExp) {CurrentToken = currentToken};
                default:
                    return multExp;
            }
        }

        private ExpressionNode MultiplicativeExpression()
        {
            var unary = UnaryExpression();
            return MultiplicativeExpressionHelper(unary);
        }

        private ExpressionNode MultiplicativeExpressionHelper(ExpressionNode unary)
        {
            var currentToken = _currentToken;
            ExpressionNode unary2;
            ExpressionNode multExp;
            switch (_currentToken.Type)
            {
                case TokenTypes.OpMul:
                    ConsumeToken();
                    unary2 = UnaryExpression();
                    multExp = MultiplicativeExpressionHelper(unary2);
                    return new MultiplicativeMulNode(unary, multExp) {CurrentToken = currentToken};
                case TokenTypes.OpDiv:
                    ConsumeToken();
                    unary2 = UnaryExpression();
                    multExp = MultiplicativeExpressionHelper(unary2);
                    return new MultiplicativeDivNode(unary, multExp) {CurrentToken = currentToken};
                case TokenTypes.OpMod:
                    ConsumeToken();
                    unary2 = UnaryExpression();
                    multExp = MultiplicativeExpressionHelper(unary2);
                    return new MultiplicativeModNode(unary, multExp) {CurrentToken = currentToken};
                default:
                    return unary;
            }
        }

        private ExpressionNode UnaryExpression()
        {
            var currentToken = _currentToken;
            ExpressionNode unaryExp;
            if (_unaryOperators.Contains(_currentToken.Type) || _currentToken.Type == TokenTypes.OpInc
                || _currentToken.Type == TokenTypes.OpDec)
            {
                var unaryOperator = UnaryOperator();
                unaryOperator.CurrentToken = currentToken;
                unaryExp = UnaryExpression();
                unaryOperator.Operand = unaryExp;
                return unaryOperator;
            }
            if (_primaryExpression.Contains(_currentToken.Type))
                return PostfixExpression();
            switch (_currentToken.Type)
            {
                case TokenTypes.OpInc:
                    ConsumeToken();
                    unaryExp = UnaryExpression();
                    return new PrefixIncrement(unaryExp) {CurrentToken = currentToken};
                case TokenTypes.OpDec:
                    ConsumeToken();
                    unaryExp = UnaryExpression();
                    return new PrefixDecrement(unaryExp) {CurrentToken = currentToken};
                default:
                    throw new ParserException("Unary expression expected.");
            }
        }

        private UnaryOperatorNode UnaryOperator()
        {
            switch (_currentToken.Type)
            {
                case TokenTypes.OpBitAnd:
                    ConsumeToken();
                    return new UnaryAndNode();
                case TokenTypes.OpMul:
                    ConsumeToken();
                    return new UnaryMulNode();
                case TokenTypes.OpSum:
                    ConsumeToken();
                    return new UnarySumNode();
                case TokenTypes.OpSub:
                    ConsumeToken();
                    return new UnarySubNode();
                case TokenTypes.OpBitComp:
                    ConsumeToken();
                    return new UnaryCompNode();
                case TokenTypes.OpLogNot:
                    ConsumeToken();
                    return new UnaryNotNode();
                default:
                    throw new ParserException("unary operator Expected.");
            }
        }

        private ExpressionNode PostfixExpression()
        {
            if (_currentToken.Type == TokenTypes.GetQuery)
                return GetQuery();
            else if (_currentToken.Type == TokenTypes.GetForm)
                return GetForm();
            else if (_currentToken.Type == TokenTypes.GetMethod)
                return GetMethod();
            else if(_currentToken.Type == TokenTypes.Atoi)
                return GetAtoi();
            else
            {
                var primaryExp = PrimaryExpression();
                return PostfixExpressionHelper(primaryExp);
            }
        }

        private ExpressionNode GetAtoi()
        {
            ConsumeToken();
            OpenParenthesis();
            var exp = Expression();
            CloseParenthesis();
            return new Atoifunc(exp);
        }

        private ExpressionNode GetMethod()
        {
            ConsumeToken();
            OpenParenthesis();
            CloseParenthesis();
            return new GetMethodFunc();
        }

        private ExpressionNode GetForm()
        {
            ConsumeToken();
            OpenParenthesis();
            var exp = Expression();
            CloseParenthesis();
            return new GetFormFunc(exp);
        }

        private ExpressionNode GetQuery()
        {
            ConsumeToken();
            OpenParenthesis();
            var exp = Expression();
            CloseParenthesis();
            return new GetQueryFunc(exp);
        }

        private ExpressionNode PostfixExpressionHelper(ExpressionNode primaryExp)
        {
            var currentToken = _currentToken;
            switch (_currentToken.Type)
            {
                case TokenTypes.OpenParenthesis:
                    ConsumeToken();
                    var assigExp = _expressionStmt.Contains(_currentToken.Type)?Expression():null;
                    CloseParenthesis();
                    return new FunctionCallNode(primaryExp, assigExp) {CurrentToken = currentToken};
                case TokenTypes.OpInc:
                    ConsumeToken();
                    return new PostfixIncrement(primaryExp) {CurrentToken = currentToken};
                case TokenTypes.OpDec:
                    ConsumeToken();
                    return new PostfixDecrement(primaryExp) {CurrentToken = currentToken};
                default:
                    return primaryExp;
            }
        }

        private ExpressionNode PrimaryExpression()
        {
            if (_currentToken.Type == TokenTypes.Id)
            {
                var current = _currentToken;
                var id = ConsumeToken();
                return AccessLinkHelper(new IdNode(id, current));
            }
            else if (_literals.Contains(_currentToken.Type))
            {
                var literal = GetLiteral();
                return literal;
            }
            else if (_currentToken.Type == TokenTypes.OpenParenthesis)
            {
                ConsumeToken();
                var exp = Expression();
                CloseParenthesis();
                return AccessLinkHelper(exp);
            }
            else
                throw new ParserException("Primary Condition Expected.");
        }

        private ExpressionNode AccessLink(ExpressionNode exp)
        {
            ExpressionNode id;
            ExpressionNode accessLink;
            switch (_currentToken.Type)
            {
                case TokenTypes.OpenArr:
                    ConsumeToken();
                    var exp2 = Expression();
                    CloseArray();
                    accessLink = AccessLinkHelper(exp2);
                    return new BracketLinkNode(exp, accessLink);
                case TokenTypes.DeRefArrow:
                    ConsumeToken();
                    id = GetId();
                    accessLink = AccessLinkHelper(id);
                    return new ArrowLinkNode(exp, accessLink);
                case TokenTypes.DeRefDot:
                    ConsumeToken();
                    id = GetId();
                    accessLink = AccessLinkHelper(id);
                    return new DotLinkNode(exp, accessLink);
                default:
                    throw new ParserException("access link ([], ->, .) Expected.");
            }
        }

        private ExpressionNode AccessLinkHelper(ExpressionNode exp)
        {
            var currentToken = _currentToken;
            if (_accessLinks.Contains(_currentToken.Type))
            {
                var accessLink = AccessLink(exp);
                accessLink.CurrentToken = currentToken;
                return AccessLinkHelper(accessLink);
            }
            else
            {
                return exp;
            }
        }
    }
}
