﻿using System;
using System.Runtime.Serialization;

namespace ProyectoCompi1
{
    [Serializable]
    internal class SemanticException : Exception
    {
        public SemanticException()
        {
        }

        public SemanticException(string message, Token currentToken): base(message + " " + currentToken)
        {
        }

        private SemanticException(string message) : base(message)
        {
        }

        public SemanticException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SemanticException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}