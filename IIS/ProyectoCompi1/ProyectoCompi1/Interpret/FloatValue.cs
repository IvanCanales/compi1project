﻿namespace ProyectoCompi1.Interpret
{
    public class FloatValue: ValueNode
    {
        public float Value;
        public override ValueNode Clone()
        {
            return new FloatValue {Value = Value};
        }
    }
}