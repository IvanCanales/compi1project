﻿namespace ProyectoCompi1.Interpret
{
    public class IntegerValue: ValueNode
    {
        public int Value;
        public override ValueNode Clone()
        {
            return new IntegerValue {Value = Value};
        }
    }
}