﻿namespace ProyectoCompi1.Interpret
{
    public class VariableValue: ValueNode
    {
        public ValueNode Value;

        public VariableValue(ValueNode value)
        {
            this.Value = value;
        }

        public override ValueNode Clone()
        {
            return new VariableValue(Value);
        }
    }
}