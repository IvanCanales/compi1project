﻿namespace ProyectoCompi1.Interpret
{
    public class StringValue: ValueNode
    {
        public string Value;
        public override ValueNode Clone()
        {
            return new StringValue {Value = this.Value};
        }
    }
}