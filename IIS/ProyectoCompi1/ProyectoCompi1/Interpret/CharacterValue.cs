﻿namespace ProyectoCompi1.Interpret
{
    public class CharacterValue: ValueNode
    {
        public char Value;
        public override ValueNode Clone()
        {
            return new CharacterValue {Value = this.Value};
        }
    }
}