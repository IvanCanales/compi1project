﻿namespace ProyectoCompi1.Interpret
{
    public class ReturnValue: ValueNode
    {
        public ValueNode Value;

        public ReturnValue(ValueNode value)
        {
            Value = value;
        }

        public ReturnValue()
        {
            
        }

        public override ValueNode Clone()
        {
            return new ReturnValue() {Value = Value};
        }
    }
}