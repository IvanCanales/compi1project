﻿using System.Collections.Generic;
using System.ComponentModel;

namespace ProyectoCompi1.Interpret
{
    public class EnumValue: ValueNode
    {
        public List<string> Enumerators;
        public List<int> Values;
        public override ValueNode Clone()
        {
            return new EnumValue {Enumerators = new List<string>(Enumerators), Values = new List<int>(Values)};
        }
    }
}