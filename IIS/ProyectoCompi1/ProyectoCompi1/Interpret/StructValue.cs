﻿using System.Collections.Generic;

namespace ProyectoCompi1.Interpret
{
    public class StructValue: ValueNode
    {
        public List<ValueNode> Attributes;
        public override ValueNode Clone()
        {
            return new StructValue {Attributes = new List<ValueNode>(Attributes)};
        }
    }
}