﻿namespace ProyectoCompi1.Interpret
{
    public class BooleanValue: ValueNode
    {
        public bool Value;
        public override ValueNode Clone()
        {
            return new BooleanValue {Value = this.Value};
        }
    }
}
