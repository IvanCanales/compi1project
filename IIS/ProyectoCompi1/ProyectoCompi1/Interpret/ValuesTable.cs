﻿using System.Collections.Generic;
using System.Linq;

namespace ProyectoCompi1.Interpret
{
    public class ValuesTable
    {
        private static ValuesTable _instance;
        private List<Dictionary<string, VariableValue>> _variables;
        private Dictionary<string, FunctionValue> _functionDecl;

        private ValuesTable()
        {
            _variables = new List<Dictionary<string, VariableValue>>
            {
                new Dictionary<string, VariableValue>()
            };
            _functionDecl = new Dictionary<string, FunctionValue>();
        }

        public void Destroyer()
        {
            _instance = new ValuesTable();
        }

        public static ValuesTable Instance
        {
            get
            {
                if (_instance != null) return _instance;
                _instance = new ValuesTable();
                return _instance;
            }
        }

        public void DeclareVariable(string name, VariableValue variable)
        {
            _variables.Last().Add(name, variable);
        }

        public ValueNode GetVariableValue(string name)
        {
            for (var i = _variables.Count - 1; i >= 0; i--)
            {
                ValueNode currentVar = null;
                if (_variables[i].ContainsKey(name))
                    currentVar = _variables[i][name].Value;
                if (currentVar != null)
                    return currentVar;
            }
            return null;
        }

        public ValueNode SetVariableValue(string name, ValueNode value)
        {
            for (var i = _variables.Count - 1; i >= 0; i--)
            {
                if (!_variables[i].ContainsKey(name)) continue;
                _variables[i][name].Value = value;
                return value;
            }
            return null;
        }

        public void IncrementStack()
        {
            _variables.Add(new Dictionary<string, VariableValue>());
        }

        public void DecrementStack()
        {
            _variables.Remove(_variables.Last());
        }

        public void DeclareFunction(string name, List<string> parameters, StatementNode code)
        {
            _functionDecl.Add(name, new FunctionValue(parameters, code));
        }

        public ValueNode CallFunction(string funcName, List<ValueNode> parameters)
        {
            IncrementStack();
            var function = _functionDecl[funcName];
            for (var i = 0; i < function.Parameters.Count; i++)
                DeclareVariable(function.Parameters[i], new VariableValue(parameters[i]));
            var value = function.ExecFunc();
            DecrementStack();
            return value;
        }
    }
}
