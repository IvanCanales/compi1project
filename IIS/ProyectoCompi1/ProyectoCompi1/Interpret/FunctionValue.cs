﻿using System.Collections.Generic;
using System.Xml.Xsl.Runtime;

namespace ProyectoCompi1.Interpret
{
    class FunctionValue: ValueNode
    {
        public StatementNode Code;
        public List<string> Parameters;

        public FunctionValue(List<string> parameters, StatementNode code)
        {
            Parameters = parameters;
            Code = code;
        }

        private FunctionValue()
        {

        }

        public override ValueNode Clone()
        {
            return new FunctionValue
            {
                Parameters = Parameters,
                Code = Code
            };
        }

        public ValueNode ExecFunc()
        {
            if (Code == null) return new VoidValue();

            var values = Code.Interpret();
            foreach (var valueNode in values)
            {
                var returnValue = valueNode as ReturnValue;
                if (returnValue != null)
                    return returnValue.Value;
            }
            return new VoidValue();
        }
    }
}
