﻿namespace ProyectoCompi1.Interpret
{
    public class DateValue: ValueNode
    {
        public string Day;
        public string Month;
        public string Year;
        public string Date;

        public DateValue(string date)
        {
            Date = date;
            var dates = date.Split('-');
            Day = dates[0];
            Month = dates[1];
            Year = dates[2];
        }

        public DateValue()
        {
        }

        public override ValueNode Clone()
        {
            return new DateValue {Date = Date, Day = this.Day, Month = this.Month, Year = this.Year};
        }
    }
}