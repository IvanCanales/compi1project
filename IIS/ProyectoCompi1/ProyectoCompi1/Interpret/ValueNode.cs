﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCompi1.Interpret
{
    public abstract class ValueNode
    {
        public abstract ValueNode Clone();
    }
}
